package com.diplom.util;

public class UrlPath {
    public static final String CREDIT_CARDS = "creditCards";
    public static final String REDIRECT_USER = "redirect:/user";
    public static final String TRANSFER = "transfer";
    public static final String REDIRECT_USER_DEPOSITS = "redirect:/user/deposits";
    public static final String REDIRECT_OPERATOR = "redirect:/operator";
    public static final String OPERATOR_EDIT_TARIFF = "operator/deposit/operatorEditTariff";
    public static final String REGISTRATION = "registration";

    private UrlPath() {

    }
}
