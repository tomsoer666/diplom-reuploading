package com.diplom.util;

import com.diplom.enums.MoneyType;

import java.math.BigDecimal;

public class MoneyRate {
    private static BigDecimal dollarRate = BigDecimal.valueOf(70);
    private static BigDecimal euroRate = BigDecimal.valueOf(90);

    private MoneyRate() {
    }

    public static BigDecimal getDollarRate() {
        return dollarRate;
    }

    public static void setDollarRate(BigDecimal dollarRate) {
        MoneyRate.dollarRate = dollarRate;
    }

    public static BigDecimal getEuroRate() {
        return euroRate;
    }

    public static void setEuroRate(BigDecimal euroRate) {
        MoneyRate.euroRate = euroRate;
    }

    public static BigDecimal convertToRuble(BigDecimal decimal, MoneyType moneyType, BigDecimal creditAmount) {
        if (moneyType == MoneyType.RUBLE) {
            decimal = decimal.add(creditAmount);
        } else if (moneyType == MoneyType.EURO) {
            decimal = decimal.add(creditAmount.multiply(euroRate));
        } else if (moneyType == MoneyType.DOLLAR) {
            decimal = decimal.add(creditAmount.multiply(dollarRate));
        }
        return decimal;
    }
}
