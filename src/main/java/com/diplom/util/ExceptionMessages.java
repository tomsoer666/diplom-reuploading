package com.diplom.util;

public class ExceptionMessages {
    public static final String NOT_ENOUGH_CASH_IN_ACCOUNT = "Not enough cash in account";

    private ExceptionMessages() {

    }
}
