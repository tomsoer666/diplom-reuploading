package com.diplom.util;

import java.math.BigDecimal;

public class MaximumPenalties {
    public static final BigDecimal MAX_PENALTIES = BigDecimal.valueOf(0.4);

    private MaximumPenalties() {
    }
}
