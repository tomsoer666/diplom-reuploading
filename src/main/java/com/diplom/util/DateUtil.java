package com.diplom.util;

public class DateUtil {
    public static final long ONE_YEAR = 365L;
    public static final long LEAP_YEAR = 366L;
    public static final long FOUR_YEAR = 365L * 3L + 366L;
    public static final long MONTH = 30L;

    private DateUtil() {
    }
}
