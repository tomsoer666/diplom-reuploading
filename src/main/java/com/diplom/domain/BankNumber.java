package com.diplom.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "bank_number")
public class BankNumber {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 6, unique = true)
    private String number;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_fk")
    private Bank bank;

    public BankNumber() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }
}
