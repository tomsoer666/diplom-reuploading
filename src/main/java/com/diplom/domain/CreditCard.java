package com.diplom.domain;

import com.diplom.enums.CreditCardStatus;
import com.diplom.enums.MoneyType;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "credit_card")
public class CreditCard {
    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal cash;
    @Pattern(regexp = "\\d{6}|\\d{12}", message = "Must be min 6 or 12 digits")
    @Column(unique = true, length = 12)
    private String cardNumber;
    private BigDecimal creditLimitSum;
    private BigDecimal interestRate;
    @Enumerated(EnumType.STRING)
    private MoneyType moneyType;
    private Date dateEnd;
    @Enumerated(EnumType.STRING)
    private CreditCardStatus creditCardStatus;
    @ManyToOne
    private BankAccount bankAccount;
    @OneToMany(mappedBy = "creditCard",
            cascade = CascadeType.ALL)
    private Set<Credit> credits;

    public CreditCard() {
    }

    public MoneyType getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(MoneyType moneyType) {
        this.moneyType = moneyType;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCreditLimitSum() {
        return creditLimitSum;
    }

    public void setCreditLimitSum(BigDecimal creditLimitSum) {
        this.creditLimitSum = creditLimitSum;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public CreditCardStatus getCreditCardStatus() {
        return creditCardStatus;
    }

    public void setCreditCardStatus(CreditCardStatus creditCardStatus) {
        this.creditCardStatus = creditCardStatus;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Set<Credit> getCredits() {
        return credits;
    }

    public void setCredits(Set<Credit> credits) {
        this.credits = credits;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
