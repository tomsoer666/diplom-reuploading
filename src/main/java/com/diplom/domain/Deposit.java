package com.diplom.domain;

import com.diplom.enums.DepositStatus;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "deposit")
public class Deposit {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tariff_id",
            foreignKey = @ForeignKey(name = "tariff_id_fk"))
    private Tariff tariff;
    @Min(value = 0, message = "Must be positive")
    private BigDecimal balance;
    @Min(value = 0, message = "Must be positive")
    private BigDecimal entryFee;
    @Min(value = 0, message = "Must be positive")
    private BigDecimal minMonthBalance;
    @Min(value = 0, message = "Must be positive")
    private BigDecimal capitalizeRate;
    private Date dateEnd;
    @Enumerated(EnumType.STRING)
    private DepositStatus depositStatus;
    private Boolean automaticRenewal;
    @ManyToOne
    private Client client;

    public Deposit() {
    }

    public BigDecimal getCapitalizeRate() {
        return capitalizeRate;
    }

    public void setCapitalizeRate(BigDecimal capitalizeRate) {
        this.capitalizeRate = capitalizeRate;
    }

    public BigDecimal getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(BigDecimal entryFee) {
        this.entryFee = entryFee;
    }

    public BigDecimal getMinMonthBalance() {
        return minMonthBalance;
    }

    public void setMinMonthBalance(BigDecimal minMonthBalance) {
        this.minMonthBalance = minMonthBalance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public DepositStatus getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(DepositStatus depositStatus) {
        this.depositStatus = depositStatus;
    }

    public Boolean getAutomaticRenewal() {
        return automaticRenewal;
    }

    public void setAutomaticRenewal(Boolean automaticRenewal) {
        this.automaticRenewal = automaticRenewal;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
