package com.diplom.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "credit_transfer")
public class CreditTransfer {
    @Id
    @GeneratedValue
    private Long id;
    @Min(value = 0L, message = "must be positive")
    private BigDecimal paymentAmount;
    private Date paymentDate;
    @ManyToOne
    private Credit credit;

    public CreditTransfer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }
}
