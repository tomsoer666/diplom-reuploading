package com.diplom.domain;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Entity
@Table(name = "tariff")
public class Tariff {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    @Min(value = 0, message = "Must be positive")
    private BigDecimal minCash;
    @Column(nullable = false)
    @Min(value = 0, message = "Must be positive")
    private BigDecimal maxCash;
    @Column(nullable = false)
    @Min(value = 1, message = "Must be positive")
    private Integer month;
    @Column(nullable = false)
    @Min(value = 0, message = "Must be positive")
    @Max(value = 1, message = "Must be less than 1")
    private BigDecimal interestRate;
    @Column(nullable = false)
    private Boolean closable;
    @Column(nullable = false)
    private Boolean replenishable;
    @Column(nullable = false)
    private Boolean withdrawable;
    @Column(nullable = false)
    private Boolean capitalized;

    public Tariff() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMinCash() {
        return minCash;
    }

    public void setMinCash(BigDecimal minCash) {
        this.minCash = minCash;
    }

    public BigDecimal getMaxCash() {
        return maxCash;
    }

    public void setMaxCash(BigDecimal maxCash) {
        this.maxCash = maxCash;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public Boolean getClosable() {
        return closable;
    }

    public void setClosable(Boolean closable) {
        this.closable = closable;
    }

    public Boolean getReplenishable() {
        return replenishable;
    }

    public void setReplenishable(Boolean replenishable) {
        this.replenishable = replenishable;
    }

    public Boolean getWithdrawable() {
        return withdrawable;
    }

    public void setWithdrawable(Boolean withdrawable) {
        this.withdrawable = withdrawable;
    }

    public Boolean getCapitalized() {
        return capitalized;
    }

    public void setCapitalized(Boolean capitalized) {
        this.capitalized = capitalized;
    }
}
