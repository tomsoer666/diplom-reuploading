package com.diplom.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private Long accountNumber;
    @Min(value = 0, message = "Must be positive")
    private BigDecimal cash;
    @ManyToOne
    private Client client;
    @ManyToOne
    private Bank bank;
    @OneToMany(mappedBy = "sender")
    private Set<Transfer> senderTransfers;
    @OneToMany(mappedBy = "receiver",
            fetch = FetchType.EAGER)
    private Set<Transfer> receiverTransfers;
    @OneToMany(mappedBy = "bankAccount",
            cascade = CascadeType.ALL)
    private Set<CreditCard> creditCards;

    public BankAccount() {
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client user) {
        this.client = user;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Set<Transfer> getSenderTransfers() {
        return senderTransfers;
    }

    public void setSenderTransfers(Set<Transfer> senderTransfers) {
        this.senderTransfers = senderTransfers;
    }

    public Set<Transfer> getReceiverTransfers() {
        return receiverTransfers;
    }

    public void setReceiverTransfers(Set<Transfer> receiverTransfers) {
        this.receiverTransfers = receiverTransfers;
    }

    public Set<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(Set<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }
}
