package com.diplom.domain;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "bank")
public class Bank {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String name;
    @Min(value = 0L, message = "Must be more than 0")
    private Long bik;
    @Min(value = 0L, message = "Must be more than 0")
    @Max(value = 1, message = "Must be less than 1")
    private BigDecimal commission;
    @OneToMany(mappedBy = "bank",
            cascade = CascadeType.ALL)
    private Set<BankAccount> bankAccounts;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_number_fk")
    private BankNumber bankNumber;

    public Bank() {
    }

    public BankNumber getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(BankNumber bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBik() {
        return bik;
    }

    public void setBik(Long bik) {
        this.bik = bik;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }
}
