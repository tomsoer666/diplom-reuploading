package com.diplom.domain;

import com.diplom.enums.TransferStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "transfer")
public class Transfer {
    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal transferAmount;
    @Enumerated(EnumType.STRING)
    private TransferStatus transferStatus;
    private Date transferDate;
    @ManyToOne
    @JoinColumn(name = "sender_id")
    private BankAccount sender;
    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private BankAccount receiver;

    public Transfer() {
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public TransferStatus getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(TransferStatus transferStatus) {
        this.transferStatus = transferStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(BigDecimal transferAmount) {
        this.transferAmount = transferAmount;
    }

    public BankAccount getSender() {
        return sender;
    }

    public void setSender(BankAccount sender) {
        this.sender = sender;
    }

    public BankAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(BankAccount receiver) {
        this.receiver = receiver;
    }
}
