package com.diplom.domain;

import com.diplom.enums.CreditStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "credit")
public class Credit {
    @Id
    @GeneratedValue
    private Long id;
    private BigDecimal creditAmount;
    private BigDecimal creditAmountWithInterestRate;
    private Date creditTerm;
    private BigDecimal creditOverpayment;
    private BigDecimal penalties;
    @Enumerated(EnumType.STRING)
    private CreditStatus creditStatus;
    @ManyToOne
    private CreditCard creditCard;
    @OneToMany(mappedBy = "credit",
            cascade = CascadeType.ALL)
    private Set<CreditTransfer> creditTransfers;

    public Credit() {
    }

    public BigDecimal getCreditAmountWithInterestRate() {
        return creditAmountWithInterestRate;
    }

    public void setCreditAmountWithInterestRate(BigDecimal creditAmountWithInterestRate) {
        this.creditAmountWithInterestRate = creditAmountWithInterestRate;
    }

    public CreditStatus getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(CreditStatus creditStatus) {
        this.creditStatus = creditStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public Date getCreditTerm() {
        return creditTerm;
    }

    public void setCreditTerm(Date creditTerm) {
        this.creditTerm = creditTerm;
    }

    public BigDecimal getCreditOverpayment() {
        return creditOverpayment;
    }

    public void setCreditOverpayment(BigDecimal creditOverpayment) {
        this.creditOverpayment = creditOverpayment;
    }

    public BigDecimal getPenalties() {
        return penalties;
    }

    public void setPenalties(BigDecimal penalties) {
        this.penalties = penalties;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public Set<CreditTransfer> getCreditTransfers() {
        return creditTransfers;
    }

    public void setCreditTransfers(Set<CreditTransfer> creditTransfers) {
        this.creditTransfers = creditTransfers;
    }
}
