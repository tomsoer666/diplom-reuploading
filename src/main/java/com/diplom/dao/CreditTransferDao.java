package com.diplom.dao;

import com.diplom.domain.CreditTransfer;

import java.math.BigDecimal;

public interface CreditTransferDao {
    CreditTransfer save(CreditTransfer creditTransfer);

    BigDecimal findCreditPaymentsByCredit(Long id);
}
