package com.diplom.dao;

import com.diplom.domain.BankAccount;
import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CreditCardDto;
import com.diplom.enums.CreditCardStatus;
import com.diplom.enums.MoneyType;

import java.util.Set;

public interface CreditCardDao {
    CreditCard findCreditCardForAccept(Long creditCardId);

    CreditCard findById(Long creditCardId);

    CreditCard save(CreditCard creditCard);

    CreditCard saveAcceptedCard(CreditCard creditCard);

    CreditCard saveDeclinedCard(CreditCard creditCard);

    Set<CreditCard> getCreditCardByStatusAndClient(CreditCardStatus cardStatus);

    CreditCard findByCardNumber(String number);

    CreditCard findByCreditCard(Long creditCardId);

    Set<CreditCard> findByCreditCardStatus(CreditCardStatus openCreditCard);
}
