package com.diplom.dao;

import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CreditDto;
import com.diplom.enums.CreditStatus;
import com.diplom.enums.MoneyType;

import java.util.Set;

public interface CreditDao {
    Set<Credit> getCreditsExceptClosed();

    Credit save(Credit credit);

    Credit findByIdAndClient(Long creditId);

    Set<Credit> findByCreditCardAndCreditStatus(CreditCard creditCard, CreditStatus status);

    Set<Credit> findByCreditCard(CreditCard creditCard);

    Credit findById(Long creditId);

    Set<Credit> findByCreditStatus(CreditStatus creditStatus);

    MoneyType findByCredit(Credit credit);
}
