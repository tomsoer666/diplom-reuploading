package com.diplom.dao;

import com.diplom.domain.Passport;

import java.util.Set;

public interface PassportDao {
    Passport save(Passport passport);

    Set<Passport> findAll();
}
