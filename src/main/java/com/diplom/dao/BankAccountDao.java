package com.diplom.dao;

import com.diplom.domain.BankAccount;
import com.diplom.domain.User;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

public interface BankAccountDao {
    Optional<BankAccount> findByNumber(Long number);

    BankAccount findByCreditCard(Long creditCard);

    BigDecimal findCommissionByCreditCardId(Long creditCardId);

    Set<BankAccount> findAll();

    Optional<BankAccount> findById(Long bankAccountId);

    BankAccount save(BankAccount bankAccount);

    Set<BankAccount> findAllByUser(User user);

    BankAccount findByBankAcc(Long senderAccount);
}
