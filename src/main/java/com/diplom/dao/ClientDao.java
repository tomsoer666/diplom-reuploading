package com.diplom.dao;

import com.diplom.domain.Client;

public interface ClientDao {
    Client findByBankAccount(Long bankAccountId);
}
