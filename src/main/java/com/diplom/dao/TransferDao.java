package com.diplom.dao;

import com.diplom.domain.Transfer;
import com.diplom.enums.TransferStatus;

import java.util.Set;

public interface TransferDao {
    Set<Transfer> findAll();

    Transfer findById(Long id);

    Transfer save(Transfer transfer);

    Set<Transfer> findByTransferStatus(TransferStatus transferStatus);

    Set<Transfer> findByClient(Long id);
}
