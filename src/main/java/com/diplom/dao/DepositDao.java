package com.diplom.dao;

import com.diplom.domain.Deposit;

import java.util.Set;

public interface DepositDao {
    Set<Deposit> getClientDeposits();

    Set<Deposit> getAllOpenedDeposits();

    Set<Deposit> getOpenDeposits();

    Deposit save(Deposit deposit);

    Deposit findDepositByClient(Long depositId);
}
