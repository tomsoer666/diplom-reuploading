package com.diplom.dao.impl;

import com.diplom.domain.Passport;
import com.diplom.repository.PassportRepository;
import com.diplom.dao.PassportDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PassportDaoImpl implements PassportDao {
    private final PassportRepository repository;

    @Autowired
    public PassportDaoImpl(PassportRepository repository) {
        this.repository = repository;
    }

    @Override
    public Passport save(Passport passport) {
        return repository.save(passport);
    }

    @Override
    public Set<Passport> findAll() {
        return new HashSet<>(repository.findAll());
    }
}
