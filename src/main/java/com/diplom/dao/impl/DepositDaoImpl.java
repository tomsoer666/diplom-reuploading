package com.diplom.dao.impl;

import com.diplom.domain.Deposit;
import com.diplom.enums.DepositStatus;
import com.diplom.repository.DepositRepository;
import com.diplom.dao.DepositDao;
import com.diplom.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class DepositDaoImpl implements DepositDao {
    private final DepositRepository depositRepository;
    private final UserService userService;

    @Autowired
    public DepositDaoImpl(DepositRepository depositRepository, UserService userService) {
        this.depositRepository = depositRepository;
        this.userService = userService;
    }

    @Override
    public Set<Deposit> getClientDeposits() {
        return depositRepository.findByClient(userService.getUser().getClient());
    }

    @Override
    public Set<Deposit> getAllOpenedDeposits() {
        return depositRepository.findByDepositStatus(DepositStatus.OPEN);
    }

    @Override
    public Set<Deposit> getOpenDeposits() {
        return depositRepository.findByOpenDeposits(userService.getUser().getClient().getId(),
                DepositStatus.OPEN.toString());
    }

    @Override
    public Deposit save(Deposit deposit) {
        return depositRepository.save(deposit);
    }

    @Override
    public Deposit findDepositByClient(Long depositId) {
        return depositRepository.findByDepositAndClient(depositId, userService.getUser().getClient().getId())
                .orElseThrow(() -> new IllegalArgumentException("Deposit by Id not exist:" + depositId));
    }
}
