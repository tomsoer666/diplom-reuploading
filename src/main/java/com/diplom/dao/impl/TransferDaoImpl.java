package com.diplom.dao.impl;

import com.diplom.domain.Transfer;
import com.diplom.enums.TransferStatus;
import com.diplom.repository.*;
import com.diplom.dao.TransferDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TransferDaoImpl implements TransferDao {

    private final TransferRepository transferRepository;
    @Autowired
    public TransferDaoImpl(TransferRepository transferRepository) {
        this.transferRepository = transferRepository;
    }
    
    @Override
    public Set<Transfer> findAll() {
        return new HashSet<>(transferRepository.findAll());
    }
    
    @Override
    public Transfer findById(Long id) {
        return transferRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Transfer by Id not exist:" + id));
    }
    
    @Override
    public Transfer save(Transfer transfer) {
        return transferRepository.save(transfer);
    }

    @Override
    public Set<Transfer> findByTransferStatus(TransferStatus transferStatus) {
        return transferRepository.findByTransferStatus(transferStatus);
    }

    @Override
    public Set<Transfer> findByClient(Long id) {
        return transferRepository.findByClient(id);
    }
}
