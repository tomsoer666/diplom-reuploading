package com.diplom.dao.impl;

import com.diplom.dao.CreditCardDao;
import com.diplom.domain.CreditCard;
import com.diplom.enums.CreditCardStatus;
import com.diplom.exceptions.CreditCardCannotBeAcceptedException;
import com.diplom.repository.CreditCardRepository;
import com.diplom.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class CreditCardDaoImpl implements CreditCardDao {
    private final CreditCardRepository creditCardRepository;
    private final UserService userService;

    @Autowired
    public CreditCardDaoImpl(CreditCardRepository creditCardRepository, UserService userService) {
        this.creditCardRepository = creditCardRepository;
        this.userService = userService;
    }

    @Override
    public CreditCard findCreditCardForAccept(Long creditCardId) {
        CreditCard creditCard = creditCardRepository.findById(creditCardId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid credit card Id:" + creditCardId));
        if (creditCard.getCreditCardStatus() != CreditCardStatus.OPEN_CREDIT_CARD) {
            throw new CreditCardCannotBeAcceptedException("Card must have status OPEN");
        }
        return creditCard;
    }

    @Override
    public CreditCard findById(Long creditCardId) {
        return creditCardRepository.findById(creditCardId)
                .orElseThrow(() -> new IllegalArgumentException("Credit card by Id not exist:" + creditCardId));
    }

    @Override
    public CreditCard save(CreditCard creditCard) {
        return creditCardRepository.save(creditCard);
    }

    @Override
    public CreditCard saveAcceptedCard(CreditCard creditCard) {
        creditCard.setCreditCardStatus(CreditCardStatus.ACCEPT_CREDIT_CARD_CLIENT);
        creditCard.setCash(BigDecimal.valueOf(0));
        return creditCardRepository.save(creditCard);
    }

    @Override
    public CreditCard saveDeclinedCard(CreditCard creditCard) {
        creditCard.setCreditCardStatus(CreditCardStatus.DECLINE_CREDIT_CARD_CLIENT);
        return creditCardRepository.save(creditCard);
    }

    @Override
    public Set<CreditCard> getCreditCardByStatusAndClient(CreditCardStatus cardStatus) {
        return creditCardRepository.findByCreditCardStatusAndClient(cardStatus.toString(),
                userService.getUser().getClient().getId());
    }

    @Override
    public CreditCard findByCardNumber(String number) {
        return creditCardRepository.findByCardNumber(number)
                .orElseThrow(() -> new IllegalArgumentException("Credit card by number not exist:" + number));
    }

    @Override
    public CreditCard findByCreditCard(Long creditCardId) {
        return creditCardRepository.findByClientSenderCard(userService.getUser().getClient().getId(),
                creditCardId).orElseThrow(
                () -> new IllegalArgumentException("Credit card by Id not exist:" + creditCardId));
    }

    @Override
    public Set<CreditCard> findByCreditCardStatus(CreditCardStatus openCreditCard) {
        return creditCardRepository.findByCreditCardStatus(openCreditCard);
    }
}
