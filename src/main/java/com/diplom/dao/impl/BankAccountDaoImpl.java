package com.diplom.dao.impl;

import com.diplom.domain.BankAccount;
import com.diplom.domain.User;
import com.diplom.repository.BankAccountRepository;
import com.diplom.dao.BankAccountDao;
import com.diplom.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class BankAccountDaoImpl implements BankAccountDao {
    private final BankAccountRepository bankAccountRepository;
    private final UserService userService;

    @Autowired
    public BankAccountDaoImpl(BankAccountRepository bankAccountRepository, UserService userService) {
        this.bankAccountRepository = bankAccountRepository;
        this.userService = userService;
    }

    @Override
    public Optional<BankAccount> findByNumber(Long number) {
        return bankAccountRepository.findByAccountNumber(number);
    }

    @Override
    public BankAccount findByCreditCard(Long creditCard) {
        return bankAccountRepository.findByCreditCard(creditCard);
    }

    @Override
    public BigDecimal findCommissionByCreditCardId(Long creditCardId) {
        return bankAccountRepository.findCommissionByCreditCardId(creditCardId);
    }

    @Override
    public Set<BankAccount> findAll() {
        return new HashSet<>(bankAccountRepository.findAll());
    }

    @Override
    public Optional<BankAccount> findById(Long bankAccountId) {
        return bankAccountRepository.findById(bankAccountId);
    }

    @Override
    public BankAccount save(BankAccount bankAccount) {
        return bankAccountRepository.save(bankAccount);
    }

    @Override
    public Set<BankAccount> findAllByUser(User user) {
        return bankAccountRepository.findByClient(user.getClient());
    }

    @Override
    public BankAccount findByBankAcc(Long senderAccount) {
        return bankAccountRepository.findBySenderAccount(senderAccount, userService.getUser().getClient().getId())
                .orElseThrow(() -> new IllegalArgumentException("Bank Account by Id not exists:" + senderAccount));
    }
}
