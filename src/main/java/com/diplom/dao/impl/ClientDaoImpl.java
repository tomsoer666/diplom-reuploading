package com.diplom.dao.impl;

import com.diplom.dao.ClientDao;
import com.diplom.domain.Client;
import com.diplom.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientDaoImpl implements ClientDao {
    private final ClientRepository clientRepository;

    @Autowired
    public ClientDaoImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client findByBankAccount(Long bankAccountId) {
       return clientRepository.findByBankAccount(bankAccountId);
    }
}
