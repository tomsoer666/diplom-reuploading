package com.diplom.dao.impl;

import com.diplom.domain.Bank;
import com.diplom.repository.BankRepository;
import com.diplom.dao.BankDao;
import com.diplom.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class BankDaoImpl implements BankDao {
    private final BankRepository bankRepository;
    private final UserService userService;

    @Autowired
    public BankDaoImpl(BankRepository bankRepository, UserService userService) {
        this.bankRepository = bankRepository;
        this.userService = userService;
    }

    @Override
    public Bank findBankById(Long id) {
        return bankRepository.findById(id)
                .orElseThrow(()
                        -> new IllegalArgumentException
                        ("Bank by Id not exist :" + id));
    }

    @Override
    public Set<Bank> findAll() {
        return new HashSet<>(bankRepository.findAll());
    }

    @Override
    public Bank findBankByCreditCardId(Long id) {
        return bankRepository.findBankByCreditCardIdAndClient(id, userService.getUser().getClient().getId())
                .orElseThrow(() -> new IllegalArgumentException("Bank with Credit card by Id not exists :" + id));
    }

    @Override
    public Bank save(Bank bank) {
        return bankRepository.save(bank);
    }

    @Override
    public Bank findByBankAccount(Long id) {
        return bankRepository.findByBankAccount(id)
                .orElseThrow(() -> new IllegalArgumentException("Bank with Bank account by Id not exists :" + id));
    }

    @Override
    public Bank findBankByCreditCardOperator(Long id) {
        return bankRepository.findBankByCreditCardId(id)
                .orElseThrow(() -> new IllegalArgumentException("Bank with Credit card by Id not exists :" + id));
    }
}
