package com.diplom.dao.impl;

import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.enums.CreditStatus;
import com.diplom.enums.MoneyType;
import com.diplom.repository.CreditRepository;
import com.diplom.dao.CreditDao;
import com.diplom.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CreditDaoImpl implements CreditDao {
    private final CreditRepository creditRepository;
    private final UserService userService;

    @Autowired
    public CreditDaoImpl(CreditRepository creditRepository, UserService userService) {
        this.creditRepository = creditRepository;
        this.userService = userService;
    }

    @Override
    public Set<Credit> getCreditsExceptClosed() {
        Set<Credit> credits = creditRepository.findByCreditStatus(CreditStatus.NORMAL);
        credits.addAll(creditRepository.findByCreditStatus(CreditStatus.OUT_DATE));
        return credits;
    }

    @Override
    public Credit save(Credit credit) {
        return creditRepository.save(credit);
    }

    @Override
    public Credit findByIdAndClient(Long creditId) {
        return creditRepository.findByCreditIdAndClient(creditId, userService.getUser().getClient().getId())
                .orElseThrow(() -> new IllegalArgumentException("Credit by Id not exist:" + creditId));
    }

    @Override
    public Set<Credit> findByCreditCardAndCreditStatus(CreditCard creditCard, CreditStatus status) {
        return creditRepository.findByCreditCardAndCreditStatus(creditCard, status);
    }

    @Override
    public Set<Credit> findByCreditCard(CreditCard creditCard) {
        return creditRepository.findByCreditCard(creditCard);
    }

    @Override
    public Credit findById(Long creditId) {
        return creditRepository.findById(creditId)
                .orElseThrow(() -> new IllegalArgumentException("Credit by Id not exist:" + creditId));
    }

    @Override
    public Set<Credit> findByCreditStatus(CreditStatus creditStatus) {
        return creditRepository.findByCreditStatus(creditStatus);
    }

    @Override
    public MoneyType findByCredit(Credit credit) {
        return creditRepository.findByCredit(credit.getId());
    }
}
