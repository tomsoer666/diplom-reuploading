package com.diplom.dao.impl;

import com.diplom.domain.CreditTransfer;
import com.diplom.repository.CreditTransferRepository;
import com.diplom.dao.CreditTransferDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CreditTransferDaoImpl implements CreditTransferDao {
    private final CreditTransferRepository creditTransferRepository;

    @Autowired
    public CreditTransferDaoImpl(CreditTransferRepository creditTransferRepository) {
        this.creditTransferRepository = creditTransferRepository;
    }

    @Override
    public CreditTransfer save(CreditTransfer creditTransfer) {
        return creditTransferRepository.save(creditTransfer);
    }

    @Override
    public BigDecimal findCreditPaymentsByCredit(Long id) {
        return creditTransferRepository.findCreditPaymentsByCredit(id);
    }
}
