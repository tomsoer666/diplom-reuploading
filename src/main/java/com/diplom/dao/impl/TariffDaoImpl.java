package com.diplom.dao.impl;

import com.diplom.domain.Tariff;
import com.diplom.repository.TariffRepository;
import com.diplom.dao.TariffDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class TariffDaoImpl implements TariffDao {
    private final TariffRepository tariffRepository;

    @Autowired
    public TariffDaoImpl(TariffRepository tariffRepository) {
        this.tariffRepository = tariffRepository;
    }

    @Override
    public Tariff emptyTariff() {
        return new Tariff();
    }

    @Override
    public Set<Tariff> getAllTariffs() {
        return new HashSet<>(tariffRepository.findAll());
    }

    @Override
    public Tariff findById(Long id) {
        return tariffRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Tariff by Id not exist:" + id));
    }

    @Override
    public Tariff save(Tariff tariff) {
        if (tariff.getMinCash().compareTo(tariff.getMaxCash()) > 0) {
            throw new IllegalArgumentException("Min cash must be less then max");
        }
        return tariffRepository.save(tariff);
    }

    @Override
    public void delete(Long tariffId) {
        tariffRepository.deleteById(tariffId);
    }
}
