package com.diplom.dao;

import com.diplom.domain.Tariff;

import java.util.Set;

public interface TariffDao {
    Tariff emptyTariff();

    Set<Tariff> getAllTariffs();

    Tariff findById(Long id);

    Tariff save(Tariff tariff);

    void delete(Long tariffId);
}
