package com.diplom.dao;

import com.diplom.domain.Bank;

import java.util.Set;

public interface BankDao {
    Bank findBankById(Long id);

    Set<Bank> findAll();

    Bank findBankByCreditCardId(Long id);

    Bank save(Bank bank);

    Bank findByBankAccount(Long id);

    Bank findBankByCreditCardOperator(Long id);
}
