package com.diplom.exceptions;

public class DepositClosedException extends RuntimeException {

    public DepositClosedException(String message) {
        super(message);
    }
}
