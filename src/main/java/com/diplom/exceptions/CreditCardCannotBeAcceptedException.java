package com.diplom.exceptions;

public class CreditCardCannotBeAcceptedException extends RuntimeException {

    public CreditCardCannotBeAcceptedException(String message) {
        super(message);
    }
}
