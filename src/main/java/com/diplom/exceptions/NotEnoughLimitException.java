package com.diplom.exceptions;

public class NotEnoughLimitException extends RuntimeException {
    public NotEnoughLimitException(String message) {
        super(message);
    }

}
