package com.diplom.exceptions;

public class OutDatedCreditException extends RuntimeException {

    public OutDatedCreditException(String message) {
        super(message);
    }
}
