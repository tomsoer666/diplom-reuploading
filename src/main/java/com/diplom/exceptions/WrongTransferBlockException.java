package com.diplom.exceptions;

public class WrongTransferBlockException extends RuntimeException {

    public WrongTransferBlockException(String message) {
        super(message);
    }
}
