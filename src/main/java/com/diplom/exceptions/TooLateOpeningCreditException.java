package com.diplom.exceptions;

public class TooLateOpeningCreditException extends RuntimeException {

    public TooLateOpeningCreditException(String message) {
        super(message);
    }
}
