package com.diplom.exceptions;

public class CreditAlreadyPayedException extends RuntimeException {
    public CreditAlreadyPayedException(String message) {
        super(message);
    }
}
