package com.diplom.exceptions;

public class CreditCardNotOpenException extends RuntimeException {
    public CreditCardNotOpenException(String message) {
        super(message);
    }

}
