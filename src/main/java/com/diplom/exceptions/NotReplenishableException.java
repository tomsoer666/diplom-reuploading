package com.diplom.exceptions;

public class NotReplenishableException extends RuntimeException {

    public NotReplenishableException(String message) {
        super(message);
    }
}
