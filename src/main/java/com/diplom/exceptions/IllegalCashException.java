package com.diplom.exceptions;

public class IllegalCashException extends RuntimeException {

    public IllegalCashException(String message) {
        super(message);
    }
}
