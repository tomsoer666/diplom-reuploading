package com.diplom.service;

import com.diplom.domain.Bank;

import java.util.Set;

public interface BankService {
    Set<Bank> getAllBank();

    Bank getBankById(Long bankId);
}
