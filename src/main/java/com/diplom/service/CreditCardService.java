package com.diplom.service;

import com.diplom.domain.BankAccount;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CreditCardDto;

import java.util.Set;

public interface CreditCardService {
    Set<CreditCard> showAcceptedCreditCard();

    Set<CreditCard> showRequestedCreditCard();

    Set<CreditCard> showRequestOnCreditCard();

    void openCreditCard(Long bankAccountId, CreditCardDto creditCardDto);
}
