package com.diplom.service;

import com.diplom.domain.Role;

import java.util.Set;

public interface RoleService {
    Set<Role> saveDefaultUser();

    Set<Role> saveOperator();

    boolean addUserRole();

    boolean addOperatorRole();
}
