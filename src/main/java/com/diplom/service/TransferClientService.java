package com.diplom.service;

import com.diplom.dto.TransferDto;

public interface TransferClientService {
    void accountToAccountTransfer(TransferDto transferDto);

    void accountToCardTransfer(TransferDto transferDto);

    void cardToAccountTransfer(TransferDto transferDto);

    void cardToCardTransfer(TransferDto transferDto);
}
