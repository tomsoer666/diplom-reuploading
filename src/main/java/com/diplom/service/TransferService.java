package com.diplom.service;

import com.diplom.domain.Transfer;

import java.util.Set;

public interface TransferService {
    Set<Transfer> getTransfers();

    Set<Transfer> getSuspiciousTransfers();
}
