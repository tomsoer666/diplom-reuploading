package com.diplom.service;

import com.diplom.domain.Bank;
import com.diplom.domain.CreditCard;
import com.diplom.domain.Transfer;
import com.diplom.dto.CreditCardDto;
import com.diplom.dto.CreditDto;

import java.util.Set;

public interface OperatorService {

    void acceptTransfer(Long transferId);

    void declineTransfer(Long transferId);

    void acceptCreditCard(CreditCardDto creditCardDto);

    void declineCreditCard(Long creditCardId);

    void changeBankCommission(Bank bank);

    void changeCreditPenalties(CreditDto creditDto);
}
