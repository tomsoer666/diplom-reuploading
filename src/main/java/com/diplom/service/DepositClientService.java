package com.diplom.service;

import com.diplom.dto.CashDto;

import java.math.BigDecimal;

public interface DepositClientService {
    void openDeposit(Long tariffId, BigDecimal cash);

    boolean isReplenishable(Long depositId);

    boolean isWithdrawable(Long depositId);

    void replenishDeposit(CashDto cashDto);

    void withdrawDeposit(CashDto cashDto);

    void automaticRenewalDeposit(Long depositId);

    void closeDeposit(Long depositId);
}
