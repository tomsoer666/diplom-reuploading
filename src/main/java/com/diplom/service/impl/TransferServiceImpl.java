package com.diplom.service.impl;

import com.diplom.dao.TransferDao;
import com.diplom.domain.Transfer;
import com.diplom.enums.TransferStatus;
import com.diplom.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TransferServiceImpl implements TransferService {
    private final UserService userService;
    private final TransferDao transferDao;

    @Autowired
    public TransferServiceImpl(UserService userService, TransferDao transferDao) {
        this.userService = userService;
        this.transferDao = transferDao;
    }

    @Override
    public Set<Transfer> getTransfers() {
        return transferDao.findByClient(userService.getUser().getClient().getId());
    }

    @Override
    public Set<Transfer> getSuspiciousTransfers() {
        return transferDao.findByTransferStatus(TransferStatus.SUSPICIOUS);
    }
}
