package com.diplom.service.impl;

import com.diplom.domain.Client;
import com.diplom.domain.Passport;
import com.diplom.domain.User;
import com.diplom.dto.UserDto;
import com.diplom.repository.ClientRepository;
import com.diplom.repository.UserRepository;
import com.diplom.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final ClientRepository clientRepository;
    private final RoleService roleService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, ClientRepository clientRepository, RoleService roleService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.clientRepository = clientRepository;
        this.roleService = roleService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    public List<User> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(UserDto userDto) {
        User userFromDB = userRepository.findByUsername(userDto.getUsername());
        if (userFromDB != null) {
            return false;
        }
        User user = new User();
        mappingUser(userDto, user);
        Passport passport = mappingPassport(userDto);
        saveClient(userDto, user, passport);
        return true;
    }

    public User getUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByUsername(username);
    }

    private void mappingUser(UserDto userDto, User user) {
        user.setUsername(userDto.getUsername());
        user.setRoles(roleService.saveDefaultUser());
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
    }

    private void saveClient(UserDto userDto, User user, Passport passport) {
        Client client = new Client();
        mappingEntities(userDto, user, passport, client);
        clientRepository.save(client);
    }

    private void mappingEntities(UserDto userDto, User user, Passport passport, Client client) {
        client.setEmail(userDto.getEmail());
        client.setPassport(passport);
        client.setUser(user);
        user.setClient(client);
        passport.setClient(client);
    }

    private Passport mappingPassport(UserDto userDto) {
        Passport passport = new Passport();
        passport.setFirstName(userDto.getFirstName());
        passport.setSecondName(userDto.getSecondName());
        passport.setPatronymic(userDto.getPatronymic());
        passport.setSex(userDto.getSex());
        passport.setBirthDate(Date.valueOf(userDto.getBirthDate()));
        return passport;
    }
}
