package com.diplom.service.impl;

import com.diplom.dao.*;
import com.diplom.domain.BankAccount;
import com.diplom.domain.Client;
import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CashDto;
import com.diplom.dto.CreditCardDto;
import com.diplom.dto.CreditDto;
import com.diplom.enums.CreditStatus;
import com.diplom.exceptions.CreditAlreadyPayedException;
import com.diplom.service.DtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Service
public class DtoServiceImpl implements DtoService {
    private final CreditDao creditDao;
    private final BankAccountDao bankAccountDao;
    private final ClientDao clientDao;
    private final CreditCardDao creditCardDao;
    private final CreditTransferDao creditTransferDao;

    @Autowired
    public DtoServiceImpl(CreditDao creditDao, BankAccountDao bankAccountDao, ClientDao clientDao, CreditCardDao creditCardDao, CreditTransferDao creditTransferDao) {
        this.creditDao = creditDao;
        this.bankAccountDao = bankAccountDao;
        this.clientDao = clientDao;
        this.creditCardDao = creditCardDao;
        this.creditTransferDao = creditTransferDao;
    }

    @Override
    public CashDto getCashDto() {
        return new CashDto();
    }

    @Override
    public CreditCardDto getCreditCardDto() {
        return new CreditCardDto();
    }

    @Override
    public CashDto getCashDto(Long depositId) {
        return new CashDto(depositId);
    }

    @Override
    public CreditDto findByIdExceptClosed(Long creditId) {
        Credit credit = creditDao.findById(creditId);
        if (credit.getCreditStatus() == CreditStatus.CLOSED) {
            throw new CreditAlreadyPayedException("Credit already payed");
        }
        CreditDto creditDto = new CreditDto();
        creditDto.setCreditId(credit.getId());
        return creditDto;
    }

    @Override
    public CreditDto mappingCreditDto(Long creditId) {
        Credit credit = creditDao.findByIdAndClient(creditId);
        if (credit.getCreditStatus() == CreditStatus.CLOSED) {
            throw new CreditAlreadyPayedException("Credit already payed");
        }
        CreditDto creditDto = new CreditDto();
        creditDto.setCreditId(credit.getId());
        return creditDto;
    }

    @Override
    public Set<CreditCardDto> mappingCreditCardDto(Set<CreditCard> creditCards) {
        Set<CreditCardDto> creditCardDtos = new HashSet<>();
        creditCards.forEach(creditCard -> {
            BankAccount bankAccount = bankAccountDao.findByCreditCard(creditCard.getId());
            Client client = clientDao.findByBankAccount(bankAccount.getId());
            creditCardDtos.add(mappingCreditCardDtoField(creditCard, bankAccount, client));
        });
        return creditCardDtos;
    }

    private CreditCardDto mappingCreditCardDtoField(CreditCard creditCard, BankAccount bankAccount, Client client) {
        CreditCardDto creditCardDto = new CreditCardDto();
        creditCardDto.setId(creditCard.getId());
        creditCardDto.setMoneyType(creditCard.getMoneyType());
        creditCardDto.setAccountNumber(bankAccount.getAccountNumber());
        creditCardDto.setEmail(client.getEmail());
        creditCardDto.setFirstName(client.getPassport().getFirstName());
        creditCardDto.setSecondName(client.getPassport().getSecondName());
        creditCardDto.setPatronymic(client.getPassport().getPatronymic());
        creditCardDto.setBirthDate(client.getPassport().getBirthDate());
        creditCardDto.setSex(client.getPassport().getSex());
        return creditCardDto;
    }


    @Override
    public Set<CreditDto> findOpenedCredit(Long creditCardId) {
        CreditCard creditCard = creditCardDao.findByCreditCard(creditCardId);
        Set<Credit> credits = creditDao.findByCreditCardAndCreditStatus(creditCard, CreditStatus.NORMAL);
        credits.addAll(creditDao.findByCreditCardAndCreditStatus(creditCard, CreditStatus.OUT_DATE));
        Set<CreditDto> creditDto = new HashSet<>();
        credits.forEach(credit -> {
            BigDecimal creditPayments = creditTransferDao.findCreditPaymentsByCredit(credit.getId());
            creditDto.add(new CreditDto(credit, creditPayments));
        });
        return creditDto;
    }
}
