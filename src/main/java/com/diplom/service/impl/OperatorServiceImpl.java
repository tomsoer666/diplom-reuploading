package com.diplom.service.impl;

import com.diplom.dao.*;
import com.diplom.domain.*;
import com.diplom.dto.CreditCardDto;
import com.diplom.dto.CreditDto;
import com.diplom.enums.CreditCardStatus;
import com.diplom.enums.TransferStatus;
import com.diplom.exceptions.CreditCardNotOpenException;
import com.diplom.exceptions.WrongTransferBlockException;
import com.diplom.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static com.diplom.util.DateUtil.FOUR_YEAR;

@Service
public class OperatorServiceImpl implements OperatorService {
    private final CreditCardDao creditCardDao;
    private final BankDao bankDao;
    private final BankAccountDao bankAccountDao;
    private final TransferDao transferDao;
    private final CreditDao creditDao;

    @Autowired
    public OperatorServiceImpl(CreditCardDao creditCardDao, BankDao bankDao, BankAccountDao bankAccountDao, TransferDao transferDao, CreditDao creditDao) {
        this.creditCardDao = creditCardDao;
        this.bankDao = bankDao;
        this.bankAccountDao = bankAccountDao;
        this.transferDao = transferDao;
        this.creditDao = creditDao;
    }

    @Override
    public void acceptTransfer(Long transferId) {
        Transfer transfer = transferDao.findById(transferId);
        if (transfer.getTransferStatus() != TransferStatus.SUSPICIOUS) {
            throw new WrongTransferBlockException("Transfer must be suspicious to block");
        }
        sendBlockedTransfer(transfer);
        transfer.setTransferStatus(TransferStatus.OK);
        transferDao.save(transfer);
    }

    private void sendBlockedTransfer(Transfer transfer) {
        BankAccount bankAccount = transfer.getReceiver();
        bankAccount.setCash(bankAccount.getCash().add(transfer.getTransferAmount()));
        bankAccountDao.save(bankAccount);
    }

    @Override
    public void declineTransfer(Long transferId) {
        Transfer transfer = transferDao.findById(transferId);
        if (transfer.getTransferStatus() != TransferStatus.SUSPICIOUS) {
            throw new WrongTransferBlockException("Transfer must be suspicious to block");
        }
        transfer.setTransferStatus(TransferStatus.BLOCK);
        transferDao.save(transfer);
    }

    @Override
    public void acceptCreditCard(CreditCardDto creditCardDto) {
        Bank bank = bankDao.findBankByCreditCardOperator(creditCardDto.getId());
        CreditCard creditCard = creditCardDao.findById(creditCardDto.getId());
        creditCard.setCardNumber(bank.getBankNumber().getNumber() + creditCardDto.getCardNumber());
        creditCard.setCreditLimitSum(creditCardDto.getCreditLimitSum());
        creditCard.setInterestRate(creditCardDto.getInterestRate());
        creditCard.setDateEnd(new Date(Instant.now().plus(FOUR_YEAR, ChronoUnit.DAYS).toEpochMilli()));
        creditCard.setCreditCardStatus(CreditCardStatus.ACCEPT_CREDIT_CARD_OPERATOR);
        creditCardDao.save(creditCard);
    }

    @Override
    public void declineCreditCard(Long creditCardId) {
        CreditCard creditCard = creditCardDao.findById(creditCardId);
        if (creditCard.getCreditCardStatus() != CreditCardStatus.OPEN_CREDIT_CARD) {
            throw new CreditCardNotOpenException("Credit card must be in open status");
        }
        creditCard.setCreditCardStatus(CreditCardStatus.DECLINE_CREDIT_CARD_OPERATOR);
        creditCardDao.save(creditCard);
    }

    @Override
    public void changeBankCommission(Bank bank) {
        bankDao.save(bank);
    }

    @Override
    public void changeCreditPenalties(CreditDto creditDto) {
        Credit credit = creditDao.findById(creditDto.getCreditId());
        if (creditDto.getPenalties().compareTo(BigDecimal.valueOf(0.4)) > 0) {
            throw new IllegalArgumentException("Penalties must be less than 0.4");
        }
        credit.setPenalties(creditDto.getPenalties());
        creditDao.save(credit);
    }
}