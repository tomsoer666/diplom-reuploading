package com.diplom.service.impl;

import com.diplom.dao.BankAccountDao;
import com.diplom.dao.BankDao;
import com.diplom.dao.CreditCardDao;
import com.diplom.dao.TransferDao;
import com.diplom.domain.Bank;
import com.diplom.domain.BankAccount;
import com.diplom.domain.CreditCard;
import com.diplom.domain.Transfer;
import com.diplom.dto.TransferDto;
import com.diplom.enums.TransferStatus;
import com.diplom.exceptions.NotEnoughCashException;
import com.diplom.service.TransferClientService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;

import static com.diplom.util.ExceptionMessages.NOT_ENOUGH_CASH_IN_ACCOUNT;

@Service
public class TransferClientServiceImpl implements TransferClientService {
    private final BankAccountDao bankAccountDao;
    private final BankDao bankDao;
    private final CreditCardDao creditCardDao;
    private final TransferDao transferDao;

    @Autowired
    public TransferClientServiceImpl(BankAccountDao bankAccountDao, BankDao bankDao, CreditCardDao creditCardDao, TransferDao transferDao) {
        this.bankAccountDao = bankAccountDao;
        this.bankDao = bankDao;
        this.creditCardDao = creditCardDao;
        this.transferDao = transferDao;
    }

    @Override
    public void accountToAccountTransfer(TransferDto transferDto) {
        BankAccount senderBankAccount = getSenderBankAccount(transferDto);
        if (senderBankAccount.getCash().compareTo(transferDto.getCash()) < 0) {
            throw new NotEnoughCashException(NOT_ENOUGH_CASH_IN_ACCOUNT);
        }
        BankAccount receiverBankAccount = getReceiverBankAccount(transferDto);
        if (senderBankAccount.getBank().getBankNumber().getNumber()
                .equals(receiverBankAccount.getBank().getBankNumber().getNumber())) {
            saveCash(transferDto.getCash(), senderBankAccount, receiverBankAccount);
            saveTransfer(transferDto.getCash(), senderBankAccount, receiverBankAccount, TransferStatus.OK);
        } else {
            senderBankAccount.setCash(senderBankAccount.getCash().subtract(transferDto.getCash()));
            bankAccountDao.save(senderBankAccount);
            BigDecimal cash = (BigDecimal.valueOf(1).subtract(senderBankAccount.getBank().getCommission())).multiply(transferDto.getCash());
            saveTransfer(cash, senderBankAccount, receiverBankAccount, TransferStatus.SUSPICIOUS);
        }
    }


    @Override
    public void accountToCardTransfer(TransferDto transferDto) {
        BankAccount senderBankAccount = getSenderBankAccount(transferDto);
        if (senderBankAccount.getCash().compareTo(transferDto.getCash()) < 0) {
            throw new NotEnoughCashException(NOT_ENOUGH_CASH_IN_ACCOUNT);
        }
        CreditCard creditCard = creditCardDao.findByCardNumber(transferDto.getReceiverCard());
        String bankNumber = creditCard.getCardNumber().substring(0, 6);
        senderBankAccount.setBank((Bank) Hibernate
                .unproxy(bankDao.findByBankAccount(senderBankAccount.getId())));
        BankAccount receiverBankAccount = (BankAccount) Hibernate
                .unproxy(bankAccountDao.findByCreditCard(creditCard.getId()));
        if (senderBankAccount.getBank().getBankNumber().getNumber().equals(bankNumber)) {
            saveCash(transferDto.getCash(), senderBankAccount, receiverBankAccount);
            saveTransfer(transferDto.getCash(), senderBankAccount, receiverBankAccount, TransferStatus.OK);
        } else {
            senderBankAccount.setCash(senderBankAccount.getCash().subtract(transferDto.getCash()));
            bankAccountDao.save(senderBankAccount);
            BigDecimal cash = (BigDecimal.valueOf(1).subtract(senderBankAccount.getBank()
                    .getCommission())).multiply(transferDto.getCash());
            saveTransfer(cash, senderBankAccount, receiverBankAccount, TransferStatus.SUSPICIOUS);
        }
    }

    @Override
    public void cardToAccountTransfer(TransferDto transferDto) {
        CreditCard creditCard = getSenderCreditCard(transferDto);
        BankAccount senderBankAccount = (BankAccount) Hibernate
                .unproxy(bankAccountDao.findByCreditCard(creditCard.getId()));
        Bank senderBank = (Bank) Hibernate
                .unproxy(bankDao.findByBankAccount(senderBankAccount.getId()));
        if (senderBankAccount.getCash().compareTo(transferDto.getCash()) < 0) {
            throw new NotEnoughCashException(NOT_ENOUGH_CASH_IN_ACCOUNT);
        }
        String bankNumber = creditCard.getCardNumber().substring(0, 6);
        BankAccount receiverBankAccount = (BankAccount) Hibernate.unproxy(getReceiverBankAccount(transferDto));
        Bank receiverBank = (Bank) Hibernate
                .unproxy(bankDao.findByBankAccount(receiverBankAccount.getId()));

        if (bankNumber.equals(receiverBank.getBankNumber().getNumber())) {
            saveCash(transferDto.getCash(), senderBankAccount, receiverBankAccount);
            saveTransfer(transferDto.getCash(), senderBankAccount, receiverBankAccount, TransferStatus.OK);
        } else {
            senderBankAccount.setCash(senderBankAccount.getCash().subtract(transferDto.getCash()));
            bankAccountDao.save(senderBankAccount);
            BigDecimal cash = (BigDecimal.valueOf(1).subtract(senderBank.getCommission())).multiply(transferDto.getCash());
            saveTransfer(cash, senderBankAccount, receiverBankAccount, TransferStatus.SUSPICIOUS);
        }
    }

    private CreditCard getSenderCreditCard(TransferDto transferDto) {
        return creditCardDao.findByCreditCard(transferDto.getSenderCard());
    }

    @Override
    public void cardToCardTransfer(TransferDto transferDto) {
        CreditCard senderCreditCard = getSenderCreditCard(transferDto);
        BankAccount senderBankAccount = (BankAccount) Hibernate
                .unproxy(bankAccountDao.findByCreditCard(senderCreditCard.getId()));
        Bank senderBank = (Bank) Hibernate
                .unproxy(bankDao.findByBankAccount(senderBankAccount.getId()));
        if (senderBankAccount.getCash().compareTo(transferDto.getCash()) < 0) {
            throw new NotEnoughCashException(NOT_ENOUGH_CASH_IN_ACCOUNT);
        }
        CreditCard receiverCreditCard = creditCardDao.findByCardNumber(transferDto.getReceiverCard());
        BankAccount receiverBankAccount = (BankAccount) Hibernate
                .unproxy(bankAccountDao.findByCreditCard(receiverCreditCard.getId()));

        if (senderCreditCard.getCardNumber().substring(0, 5)
                .equals(receiverCreditCard.getCardNumber().substring(0, 5))) {
            saveCash(transferDto.getCash(), senderBankAccount, receiverBankAccount);
            saveTransfer(transferDto.getCash(), senderBankAccount, receiverBankAccount, TransferStatus.OK);
        } else {
            senderBankAccount.setCash(senderBankAccount.getCash().subtract(transferDto.getCash()));
            bankAccountDao.save(senderBankAccount);
            BigDecimal cash = (BigDecimal.valueOf(1).subtract(senderBank.getCommission())).multiply(transferDto.getCash());
            saveTransfer(cash, senderBankAccount, receiverBankAccount, TransferStatus.SUSPICIOUS);
        }
    }

    private BankAccount getSenderBankAccount(TransferDto transferDto) {
        return bankAccountDao.findByBankAcc(transferDto.getSenderAccount());
    }

    private BankAccount getReceiverBankAccount(TransferDto transferDto) {
        return bankAccountDao.findByNumber(transferDto.getReceiverAccount())
                .orElseThrow(() ->
                        new IllegalArgumentException("Bank account by number not exist: " + transferDto.getReceiverAccount()));
    }

    private void saveCash(BigDecimal cash, BankAccount senderBankAccount, BankAccount receiverBankAccount) {
        senderBankAccount.setCash(senderBankAccount.getCash().subtract(cash));
        receiverBankAccount.setCash(receiverBankAccount.getCash().add(cash));
        bankAccountDao.save(senderBankAccount);
        bankAccountDao.save(receiverBankAccount);
    }

    private void saveTransfer(BigDecimal cash, BankAccount senderBankAccount,
                              BankAccount receiverBankAccount, TransferStatus transferStatus) {
        Transfer transfer = new Transfer();
        transfer.setTransferAmount(cash);
        transfer.setTransferStatus(transferStatus);
        transfer.setTransferDate(new Date(System.currentTimeMillis()));
        transfer.setSender(senderBankAccount);
        transfer.setReceiver(receiverBankAccount);
        transferDao.save(transfer);
    }
}
