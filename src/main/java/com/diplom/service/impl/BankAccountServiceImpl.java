package com.diplom.service.impl;

import com.diplom.dao.BankAccountDao;
import com.diplom.dao.BankDao;
import com.diplom.domain.Bank;
import com.diplom.domain.BankAccount;
import com.diplom.domain.User;
import com.diplom.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Random;
import java.util.Set;

@Service
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountDao bankAccountDao;
    private final UserService userService;
    private final BankDao bankDao;

    @Autowired
    public BankAccountServiceImpl(BankAccountDao bankAccountDao, UserService userService, BankDao bankDao) {
        this.bankAccountDao = bankAccountDao;
        this.userService = userService;
        this.bankDao = bankDao;
    }

    @Override
    public Set<BankAccount> getBankAccountByUser() {
        return bankAccountDao.findAllByUser(userService.getUser());
    }

    @Override
    public void openingBankAccount(Long bankId) {
        Bank bank = bankDao.findBankById(bankId);
        User user = userService.getUser();
        BankAccount bankAccount = new BankAccount();
        bankAccount.setBank(bank);
        bankAccount.setClient(user.getClient());
        bankAccount.setAccountNumber(generateBankAccountNumber());
        bankAccount.setCash(BigDecimal.valueOf(0));
        bankAccountDao.save(bankAccount);
    }

    private long generateBankAccountNumber() {
        return new Random().nextLong() & Long.MAX_VALUE;
    }
}
