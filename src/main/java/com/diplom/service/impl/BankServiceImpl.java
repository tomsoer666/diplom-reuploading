package com.diplom.service.impl;

import com.diplom.dao.BankDao;
import com.diplom.domain.Bank;
import com.diplom.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class BankServiceImpl implements BankService {
    private final BankDao bankDao;

    @Autowired
    public BankServiceImpl(BankDao bankDao) {
        this.bankDao = bankDao;
    }

    @Override
    public Set<Bank> getAllBank() {
        return bankDao.findAll();
    }

    @Override
    public Bank getBankById(Long bankId) {
        return bankDao.findBankById(bankId);
    }
}
