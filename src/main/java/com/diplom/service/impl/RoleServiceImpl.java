package com.diplom.service.impl;

import com.diplom.domain.Role;
import com.diplom.enums.UserRole;
import com.diplom.repository.RoleRepository;
import com.diplom.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Set<Role> saveDefaultUser() {
        findRole(UserRole.ROLE_USER);
        Role role = roleRepository.findByUserRole(UserRole.ROLE_USER);
        return Collections.singleton(role);
    }

    @Override
    public Set<Role> saveOperator() {
        findRole(UserRole.ROLE_OPERATOR);
        Role role = roleRepository.findByUserRole(UserRole.ROLE_OPERATOR);
        return Collections.singleton(role);
    }

    @Override
    public boolean addUserRole() {
        return findRole(UserRole.ROLE_USER);
    }

    @Override
    public boolean addOperatorRole() {
        return findRole(UserRole.ROLE_OPERATOR);
    }


    private boolean findRole(UserRole userRole) {
        Role role = roleRepository.findByUserRole(userRole);
        if (role == null) {
            role = new Role();
            role.setUserRole(userRole);
            roleRepository.save(role);
            return true;
        }
        return false;
    }
}
