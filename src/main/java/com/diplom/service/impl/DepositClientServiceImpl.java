package com.diplom.service.impl;

import com.diplom.dao.DepositDao;
import com.diplom.dao.TariffDao;
import com.diplom.domain.Deposit;
import com.diplom.domain.Tariff;
import com.diplom.dto.CashDto;
import com.diplom.enums.DepositStatus;
import com.diplom.exceptions.DepositClosedException;
import com.diplom.exceptions.IllegalCashException;
import com.diplom.exceptions.NotReplenishableException;
import com.diplom.service.DepositClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static com.diplom.util.DateUtil.MONTH;

@Service
public class DepositClientServiceImpl implements DepositClientService {
    private final DepositDao depositDao;
    private final TariffDao tariffDao;
    private final UserService userService;

    @Autowired
    public DepositClientServiceImpl(DepositDao depositDao, TariffDao tariffDao, UserService userService) {
        this.depositDao = depositDao;
        this.tariffDao = tariffDao;
        this.userService = userService;
    }

    @Override
    public void openDeposit(Long tariffId, BigDecimal cash) {
        Tariff tariff = tariffDao.findById(tariffId);
        checkCash(cash, tariff);
        saveDeposit(cash, tariff);
    }

    private void checkCash(BigDecimal cash, Tariff tariff) {
        if (tariff.getMaxCash().compareTo(cash) < 0
                || tariff.getMinCash().compareTo(cash) > 0) {
            throw new IllegalCashException("Cash must be in tariff range");
        }
    }

    private void saveDeposit(BigDecimal cash, Tariff tariff) {
        Deposit deposit = new Deposit();
        deposit.setTariff(tariff);
        deposit.setClient(userService.getUser().getClient());
        deposit.setAutomaticRenewal(false);
        deposit.setBalance(cash);
        deposit.setCapitalizeRate(BigDecimal.valueOf(0));
        deposit.setEntryFee(cash);
        deposit.setDepositStatus(DepositStatus.OPEN);
        deposit.setMinMonthBalance(cash);
        deposit.setDateEnd(new Date(Instant.now()
                .plus(MONTH * tariff.getMonth(), ChronoUnit.DAYS).toEpochMilli()));
        depositDao.save(deposit);
    }


    @Override
    public boolean isReplenishable(Long depositId) {
        Deposit deposit = depositDao.findDepositByClient(depositId);
        return deposit.getTariff().getReplenishable();
    }

    @Override
    public boolean isWithdrawable(Long depositId) {
        Deposit deposit = depositDao.findDepositByClient(depositId);
        return deposit.getTariff().getWithdrawable();
    }

    @Override
    public void replenishDeposit(CashDto cashDto) {
        Deposit deposit = depositDao.findDepositByClient(cashDto.getDepositId());
        checkDepositStatus(deposit);
        if (Boolean.FALSE.equals(deposit.getTariff().getReplenishable())) {
            throw new NotReplenishableException("Tariff must include replenish status");
        }
        if (deposit.getTariff().getMaxCash().compareTo(deposit.getBalance().add(cashDto.getCash())) < 0) {
            throw new IllegalCashException("Replenish cash must not overload balance");
        }
        deposit.setBalance(deposit.getBalance().add(cashDto.getCash()));
        depositDao.save(deposit);
    }

    @Override
    public void withdrawDeposit(CashDto cashDto) {
        Deposit deposit = depositDao.findDepositByClient(cashDto.getDepositId());
        checkDepositStatus(deposit);
        if (Boolean.FALSE.equals(deposit.getTariff().getWithdrawable())) {
            throw new NotReplenishableException("Tariff must include withdrawable status");
        }
        if (deposit.getTariff().getMinCash().compareTo(deposit.getBalance().subtract(cashDto.getCash())) > 0) {
            throw new IllegalCashException("Balance must be not less then minimum cash");
        }
        deposit.setBalance(deposit.getBalance().subtract(cashDto.getCash()));
        if (deposit.getMinMonthBalance().compareTo(deposit.getBalance()) > 0) {
            deposit.setMinMonthBalance(deposit.getBalance());
        }
        depositDao.save(deposit);
    }

    private void checkDepositStatus(Deposit deposit) {
        if (deposit.getDepositStatus() == DepositStatus.CLOSED) {
            throw new DepositClosedException("Deposit closed");
        }
    }

    @Override
    public void automaticRenewalDeposit(Long depositId) {
        Deposit deposit = depositDao.findDepositByClient(depositId);
        checkDepositStatus(deposit);
        deposit.setAutomaticRenewal(!Boolean.TRUE.equals(deposit.getAutomaticRenewal()));
        depositDao.save(deposit);
    }

    @Override
    public void closeDeposit(Long depositId) {
        Deposit deposit = depositDao.findDepositByClient(depositId);
        checkDepositStatus(deposit);
        if (Boolean.FALSE.equals(deposit.getTariff().getClosable())) {
            throw new DepositClosedException("Deposit must be closable");
        }
        deposit.setDepositStatus(DepositStatus.CLOSED);
        depositDao.save(deposit);
    }
}
