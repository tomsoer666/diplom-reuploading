package com.diplom.service.impl;

import com.diplom.dao.BankAccountDao;
import com.diplom.dao.CreditCardDao;
import com.diplom.dao.CreditDao;
import com.diplom.dao.CreditTransferDao;
import com.diplom.domain.BankAccount;
import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.domain.CreditTransfer;
import com.diplom.dto.CreditDto;
import com.diplom.enums.CreditCardStatus;
import com.diplom.enums.CreditStatus;
import com.diplom.enums.MoneyType;
import com.diplom.exceptions.NotEnoughLimitException;
import com.diplom.exceptions.OutDatedCreditException;
import com.diplom.exceptions.TooLateOpeningCreditException;
import com.diplom.service.CreditClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static com.diplom.util.DateUtil.MONTH;
import static com.diplom.util.MoneyRate.convertToRuble;

@Service
public class CreditClientServiceImpl implements CreditClientService {
    private final CreditCardDao creditCardDao;
    private final BankAccountDao bankAccountDao;
    private final CreditDao creditDao;
    private final CreditTransferDao creditTransferDao;

    @Autowired
    public CreditClientServiceImpl(CreditCardDao creditCardDao, BankAccountDao bankAccountDao, CreditDao creditDao, CreditTransferDao creditTransferDao) {
        this.creditCardDao = creditCardDao;
        this.bankAccountDao = bankAccountDao;
        this.creditDao = creditDao;
        this.creditTransferDao = creditTransferDao;
    }

    @Override
    public void acceptCreditCard(Long creditCardId) {
        CreditCard creditCard = creditCardDao.findByCreditCard(creditCardId);
        creditCardDao.saveAcceptedCard(creditCard);
    }

    @Override
    public void declineCreditCard(Long creditCardId) {
        CreditCard creditCard = creditCardDao.findByCreditCard(creditCardId);
        creditCardDao.saveDeclinedCard(creditCard);
    }


    @Override
    public void openCredit(CreditDto creditDto) {
        BankAccount bankAccount = bankAccountDao.findByCreditCard(creditDto.getCreditCardId());
        CreditCard creditCard = creditCardDao.findByCreditCard(creditDto.getCreditCardId());
        if (creditCard.getCreditCardStatus() == CreditCardStatus.ACCEPT_CREDIT_CARD_CLIENT) {
            Set<Credit> outdatedCredits = creditDao.findByCreditCard(creditCard);
            checkOutDatedCredits(outdatedCredits);
            Set<Credit> creditsByCard = creditDao
                    .findByCreditCardAndCreditStatus(creditCard, CreditStatus.NORMAL);
            Credit credit = new Credit();
            checkAndSetCreditTerm(creditCard, credit);
            checkCreditAmount(creditDto, creditCard, creditsByCard);
            setCredit(creditDto, credit, creditCard);
            checkCommission(bankAccount, creditCard, credit);
            saveCredit(creditCard, credit);
        }
    }

    @Override
    public void repayCredit(CreditDto creditDto) {
        Credit credit = creditDao.findByIdAndClient(creditDto.getCreditId());
        saveCreditTransfer(creditDto, credit);
        isCreditPayed(credit);
    }

    private void checkOutDatedCredits(Set<Credit> creditsByCard) {
        for (Credit credit : creditsByCard) {
            if (credit.getCreditStatus() == CreditStatus.OUT_DATE) {
                throw new OutDatedCreditException("You can't open new credit, while you have outDate credits.");
            }
        }
    }

    private void checkCommission(BankAccount bankAccount, CreditCard creditCard, Credit credit) {
        if (creditCard.getMoneyType() != MoneyType.RUBLE) {
            BigDecimal commission = bankAccountDao.findCommissionByCreditCardId(creditCard.getId());
            bankAccount.setCash(bankAccount.getCash().add(convertToRuble(BigDecimal.valueOf(0),
                    creditCard.getMoneyType(), credit.getCreditAmount()).multiply(BigDecimal.valueOf(1).subtract(commission))));
            bankAccountDao.save(bankAccount);
        } else {
            bankAccount.setCash(bankAccount.getCash().add(credit.getCreditAmount()));
        }
    }

    private void setCredit(CreditDto creditDto, Credit credit, CreditCard creditCard) {
        credit.setCreditAmount(creditDto.getCreditAmount());
        credit.setCreditOverpayment(BigDecimal.valueOf(0));
        credit.setCreditStatus(CreditStatus.NORMAL);
        credit.setPenalties(BigDecimal.valueOf(0.01));
        credit.setCreditAmountWithInterestRate(creditDto.getCreditAmount()
                .add(creditDto.getCreditAmount().multiply(creditCard.getInterestRate())));
    }

    private void checkCreditAmount(CreditDto creditDto, CreditCard creditCard, Set<Credit> creditsByCard) {
        BigDecimal creditLimitSum = creditCard.getCreditLimitSum();
        BigDecimal creditAmountSum = BigDecimal.valueOf(0);
        for (Credit creditView : creditsByCard) {
            creditAmountSum = convertToRuble(creditAmountSum, creditCard.getMoneyType(), creditView.getCreditAmount());
        }
        creditAmountSum = convertToRuble(creditAmountSum, creditCard.getMoneyType(), creditDto.getCreditAmount());
        if (creditLimitSum.compareTo(creditAmountSum) < 0) {
            throw new NotEnoughLimitException("Credit amount exceeds credits limit");
        }
    }

    private void checkAndSetCreditTerm(CreditCard creditCard, Credit credit) {
        Instant thirtyDays = Instant.now().plus(MONTH, ChronoUnit.DAYS);
        if (Instant.ofEpochMilli(creditCard.getDateEnd().getTime()).toEpochMilli() < thirtyDays.toEpochMilli()) {
            Instant oneDay = Instant.now().plus(1, ChronoUnit.DAYS);
            if (Instant.ofEpochMilli(creditCard.getDateEnd().getTime()).toEpochMilli() < oneDay.toEpochMilli()) {
                throw new TooLateOpeningCreditException("Credit card soon will be closed. Too late for credit.");
            }
            credit.setCreditTerm(creditCard.getDateEnd());
        }
        credit.setCreditTerm(new Date(thirtyDays.toEpochMilli()));
    }

    private void saveCredit(CreditCard creditCard, Credit credit) {
        credit.setCreditCard(creditCard);
        creditDao.save(credit);
    }

    private void saveCreditTransfer(CreditDto creditDto, Credit credit) {
        CreditTransfer creditTransfer = new CreditTransfer();
        creditTransfer.setPaymentAmount(creditDto.getCreditPay());
        creditTransfer.setPaymentDate(new Date(System.currentTimeMillis()));
        creditTransfer.setCredit(credit);
        creditTransferDao.save(creditTransfer);
    }

    private void isCreditPayed(Credit credit) {
        BigDecimal creditPayments = creditTransferDao
                .findCreditPaymentsByCredit(credit.getId());
        MoneyType moneyType = creditDao.findByCredit(credit);
        if (creditPayments.compareTo(convertToRuble(BigDecimal.valueOf(0),
                moneyType, credit.getCreditAmountWithInterestRate())
                .add(credit.getCreditOverpayment())) > 0) {
            credit.setCreditStatus(CreditStatus.CLOSED);
            creditDao.save(credit);
        }
    }
}
