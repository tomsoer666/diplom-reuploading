package com.diplom.service.impl;

import com.diplom.dao.BankAccountDao;
import com.diplom.dao.CreditCardDao;
import com.diplom.domain.BankAccount;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CreditCardDto;
import com.diplom.enums.CreditCardStatus;
import com.diplom.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CreditCardServiceImpl implements CreditCardService {
    private final CreditCardDao creditCardDao;
    private final BankAccountDao bankAccountDao;

    @Autowired
    public CreditCardServiceImpl(CreditCardDao creditCardDao, BankAccountDao bankAccountDao) {
        this.creditCardDao = creditCardDao;
        this.bankAccountDao = bankAccountDao;
    }

    @Override
    public Set<CreditCard> showAcceptedCreditCard() {
        return creditCardDao.getCreditCardByStatusAndClient(CreditCardStatus.ACCEPT_CREDIT_CARD_CLIENT);
    }

    @Override
    public Set<CreditCard> showRequestedCreditCard() {
        return creditCardDao.getCreditCardByStatusAndClient(CreditCardStatus.ACCEPT_CREDIT_CARD_OPERATOR);
    }

    @Override
    public Set<CreditCard> showRequestOnCreditCard() {
        return creditCardDao.
                findByCreditCardStatus(CreditCardStatus.OPEN_CREDIT_CARD);
    }

    @Override
    public void openCreditCard(Long bankAccountId, CreditCardDto creditCardDto) {
        BankAccount bankAccount = bankAccountDao.findByBankAcc(bankAccountId);
        CreditCard card = new CreditCard();
        card.setBankAccount(bankAccount);
        card.setCreditCardStatus(CreditCardStatus.OPEN_CREDIT_CARD);
        card.setMoneyType(creditCardDto.getMoneyType());
        creditCardDao.save(card);
    }
}
