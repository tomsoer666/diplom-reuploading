package com.diplom.service;

import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CashDto;
import com.diplom.dto.CreditCardDto;
import com.diplom.dto.CreditDto;

import java.util.Set;

public interface DtoService {
    CreditDto findByIdExceptClosed(Long creditId);

    CreditDto mappingCreditDto(Long creditId);

    CashDto getCashDto();

    CreditCardDto getCreditCardDto();

    CashDto getCashDto(Long depositId);

    Set<CreditCardDto> mappingCreditCardDto(Set<CreditCard> creditCards);

    Set<CreditDto> findOpenedCredit(Long creditCardId);
}
