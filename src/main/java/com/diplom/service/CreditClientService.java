package com.diplom.service;

import com.diplom.dto.CreditDto;

public interface CreditClientService {
    void acceptCreditCard(Long creditCardId);

    void declineCreditCard(Long creditCardId);

    void openCredit(CreditDto creditDto);

    void repayCredit(CreditDto creditDto);
}
