package com.diplom.service;

import com.diplom.domain.BankAccount;

import java.util.Set;

public interface BankAccountService {
    Set<BankAccount> getBankAccountByUser();

    void openingBankAccount(Long bankId);
}
