package com.diplom.repository;

import com.diplom.domain.Client;
import com.diplom.domain.Deposit;
import com.diplom.enums.DepositStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface DepositRepository extends JpaRepository<Deposit, Long> {
    Set<Deposit> findByClient(Client client);

    @Query(value = "SELECT d.*\n" +
            "FROM public.deposit d\n" +
            "JOIN public.client ct\n" +
            "ON d.client_id = ct.id\n" +
            "WHERE d.id = ?1\n" +
            "AND ct.id = ?2",
            nativeQuery = true)
    Optional<Deposit> findByDepositAndClient(Long depositId, Long id);

    @Query(value = "SELECT d.*\n" +
            "FROM public.deposit d\n" +
            "JOIN public.client ct\n" +
            "ON d.client_id = ct.id\n" +
            "WHERE ct.id = ?1\n" +
            "AND d.deposit_status = ?2",
            nativeQuery = true)
    Set<Deposit> findByOpenDeposits(Long clientId, String status);

    Set<Deposit> findByDepositStatus(DepositStatus depositStatus);
}
