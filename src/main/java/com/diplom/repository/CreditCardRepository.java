package com.diplom.repository;

import com.diplom.domain.CreditCard;
import com.diplom.enums.CreditCardStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
    Set<CreditCard> findByCreditCardStatus(CreditCardStatus creditCardStatus);

    @Query(value = "SELECT *\n" +
            "FROM public.credit_card cc\n" +
            "JOIN public.bank_account ba\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "JOIN public.client ct\n" +
            "ON ba.client_id = ct.id\n" +
            "WHERE credit_card_status = ?1\n" +
            "AND ct.id = ?2",
            nativeQuery = true)
    Set<CreditCard> findByCreditCardStatusAndClient(String creditCardStatus, Long clientId);

    Optional<CreditCard> findByCardNumber(String cardNumber);

    @Query(value = "SELECT cc.*\n" +
            "FROM public.credit_card cc\n" +
            "JOIN public.bank_account ba\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "JOIN public.client ct\n" +
            "ON ba.client_id = ct.id\n" +
            "WHERE ct.id = ?1\n" +
            "AND cc.id = ?2",
            nativeQuery = true)
    Optional<CreditCard> findByClientSenderCard(Long clientId, Long senderCard);
}
