package com.diplom.repository;

import com.diplom.domain.Bank;
import com.diplom.domain.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;

@Repository
public interface BankRepository extends JpaRepository<Bank, Long> {
    @Query(value = "SELECT b.*\n" +
            "FROM public.bank b\n" +
            "JOIN public.bank_account ba\n" +
            "ON ba.bank_id = b.id\n" +
            "JOIN public.client ct\n" +
            "ON ba.client_id = ct.id\n" +
            "JOIN public.credit_card cc\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "WHERE cc.id = ?1\n" +
            "AND ct.id = ?2",
            nativeQuery = true)
    Optional<Bank> findBankByCreditCardIdAndClient(Long creditCardId, Long clientId);

    @Query(value = "SELECT b.*\n" +
            "FROM public.bank b\n" +
            "JOIN public.bank_account ba\n" +
            "ON ba.bank_id = b.id\n" +
            "JOIN public.credit_card cc\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "WHERE cc.id = ?1\n",
            nativeQuery = true)
    Optional<Bank> findBankByCreditCardId(Long creditCardId);

    @Query(value = "SELECT b.*\n" +
            "FROM public.bank b\n" +
            "JOIN public.bank_account ba\n" +
            "ON ba.bank_id = b.id\n" +
            "WHERE ba.id = ?1",
            nativeQuery = true)
    Optional<Bank> findByBankAccount(Long bankAccount);
}
