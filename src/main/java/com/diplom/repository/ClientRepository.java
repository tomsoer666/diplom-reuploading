package com.diplom.repository;

import com.diplom.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    @Query(value = "SELECT *\n" +
            "FROM public.client c\n" +
            "JOIN public.bank_account ba\n" +
            "ON ba.client_id = c.id\n" +
            "WHERE ba.id = ?1",
            nativeQuery = true)
    Client findByBankAccount(Long bankAccountId);
}
