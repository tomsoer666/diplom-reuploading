package com.diplom.repository;

import com.diplom.domain.CreditTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface CreditTransferRepository extends JpaRepository<CreditTransfer, Long> {
    @Query(value = "SELECT SUM(payment_amount)\n" +
            "FROM public.credit_transfer ct\n" +
            "WHERE ct.credit_id = ?1",
            nativeQuery = true)
    BigDecimal findCreditPaymentsByCredit(Long id);
}
