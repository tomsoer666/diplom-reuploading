package com.diplom.repository;

import com.diplom.domain.Role;
import com.diplom.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByUserRole(UserRole userRole);
}
