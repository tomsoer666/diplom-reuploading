package com.diplom.repository;

import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.enums.CreditStatus;
import com.diplom.enums.MoneyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Long> {
    Set<Credit> findByCreditCardAndCreditStatus(CreditCard creditCard, CreditStatus creditStatus);

    Set<Credit> findByCreditStatus(CreditStatus creditStatus);

    Set<Credit> findByCreditCard(CreditCard creditCard);

    @Query(value = "SELECT cr.*\n" +
            "FROM public.credit cr\n" +
            "JOIN public.credit_card cc\n" +
            "ON cr.credit_card_id = cc.id\n" +
            "JOIN public.bank_account ba\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "JOIN public.client ct\n" +
            "ON ba.client_id = ct.id\n" +
            "WHERE cr.id = ?1\n" +
            "AND ct.id = ?2",
            nativeQuery = true)
    Optional<Credit> findByCreditIdAndClient(Long creditId, Long id);

    @Query(value = "SELECT cc.money_type\n" +
            "FROM public.credit_card cc\n" +
            "JOIN public.credit cr\n" +
            "ON cr.credit_card_id = cc.id\n" +
            "WHERE cr.id = ?1",
            nativeQuery = true)
    MoneyType findByCredit(Long creditId);
}
