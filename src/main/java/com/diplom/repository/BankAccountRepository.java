package com.diplom.repository;

import com.diplom.domain.BankAccount;
import com.diplom.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    @Query(value = "SELECT commission\n" +
            "FROM public.bank b\n" +
            "JOIN public.bank_account ba\n" +
            "ON ba.bank_id = b.id\n" +
            "JOIN public.credit_card cc\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "WHERE cc.id = ?1",
            nativeQuery = true)
    BigDecimal findCommissionByCreditCardId(Long id);

    Set<BankAccount> findByClient(Client client);


    @Query(value = "SELECT *\n" +
            "FROM public.bank_account ba\n" +
            "JOIN public.credit_card cc\n" +
            "ON cc.bank_account_id = ba.id\n" +
            "WHERE cc.id = ?1",
            nativeQuery = true)
    BankAccount findByCreditCard(Long creditCardId);

    @Query(value = "SELECT *\n" +
            "FROM public.bank_account ba\n" +
            "JOIN public.client cc\n" +
            "ON ba.client_id = cc.id\n" +
            "WHERE cc.id = ?1\n" +
            "AND ba.account_number = ?2",
            nativeQuery = true)
    Optional<BankAccount> findByClientAccountNumber(Long clientId, Long accountNumber);

    @Query(value = "SELECT *\n" +
            "FROM public.bank_account ba\n" +
            "JOIN public.client cc\n" +
            "ON ba.client_id = cc.id\n" +
            "WHERE ba.id = ?1\n" +
            "AND cc.id = ?2",
            nativeQuery = true)
    Optional<BankAccount> findBySenderAccount(Long senderAccount, Long clientId);

    Optional<BankAccount> findByAccountNumber(Long accountNumber);
}
