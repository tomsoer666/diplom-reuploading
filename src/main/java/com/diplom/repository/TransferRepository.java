package com.diplom.repository;

import com.diplom.domain.Transfer;
import com.diplom.enums.TransferStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface TransferRepository extends JpaRepository<Transfer, Long> {
    @Query(value = "SELECT tr.*\n" +
            "FROM public.transfer tr\n" +
            "JOIN public.bank_account ba\n" +
            "ON tr.sender_id = ba.id\n" +
            "JOIN public.client ct\n" +
            "ON ba.client_id = ct.id\n" +
            "WHERE ct.id = ?1",
            nativeQuery = true)
    Set<Transfer> findByClient(Long id);

    Set<Transfer> findByTransferStatus(TransferStatus transferStatus);
}
