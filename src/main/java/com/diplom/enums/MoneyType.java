package com.diplom.enums;

public enum MoneyType {
    RUBLE,
    EURO,
    DOLLAR
}
