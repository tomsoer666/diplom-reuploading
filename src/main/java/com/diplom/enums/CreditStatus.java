package com.diplom.enums;

public enum CreditStatus {
    NORMAL,
    OUT_DATE,
    CLOSED
}
