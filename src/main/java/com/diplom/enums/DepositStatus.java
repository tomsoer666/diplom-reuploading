package com.diplom.enums;

public enum DepositStatus {
    OPEN,
    CLOSED
}
