package com.diplom.enums;

public enum TransferStatus {
    OK,
    SUSPICIOUS,
    BLOCK
}
