package com.diplom.enums;

public enum UserRole {
    ROLE_OPERATOR,
    ROLE_USER
}
