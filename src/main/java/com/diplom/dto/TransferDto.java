package com.diplom.dto;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class TransferDto {
    private Long senderAccount;
    private Long senderCard;
    private Long receiverAccount;
    private String receiverCard;
    @Min(value = 0, message = "Must be more than 0")
    private BigDecimal cash;

    public TransferDto(Long senderAccount, Long senderCard, Long receiverAccount, String receiverCard, BigDecimal cash) {
        this.senderAccount = senderAccount;
        this.senderCard = senderCard;
        this.receiverAccount = receiverAccount;
        this.receiverCard = receiverCard;
        this.cash = cash;
    }

    public TransferDto() {
    }

    public Long getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(Long senderAccount) {
        this.senderAccount = senderAccount;
    }

    public Long getSenderCard() {
        return senderCard;
    }

    public void setSenderCard(Long senderCard) {
        this.senderCard = senderCard;
    }

    public Long getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(Long receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getReceiverCard() {
        return receiverCard;
    }

    public void setReceiverCard(String receiverCard) {
        this.receiverCard = receiverCard;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }
}
