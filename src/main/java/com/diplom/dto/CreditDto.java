package com.diplom.dto;

import com.diplom.domain.Credit;
import com.diplom.enums.CreditStatus;

import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.sql.Date;

public class CreditDto {
    private Long creditCardId;
    private Long creditId;
    @Min(value = 0L, message = "Must be more than 0")
    private BigDecimal creditAmount;
    private BigDecimal creditAmountWithInterestRate;
    @Min(value = 0L, message = "Must be more than 0")
    private BigDecimal creditPay;
    private BigDecimal creditOverpayment;
    @Min(value = 0L, message = "Must be more than 0")
    private BigDecimal penalties;
    private BigDecimal creditPayments;
    private CreditStatus creditStatus;
    private Date creditTerm;

    public CreditDto() {
    }

    public CreditDto(Credit credit, BigDecimal creditPayments) {
        this.creditId = credit.getId();
        this.creditAmount = credit.getCreditAmount();
        this.creditAmountWithInterestRate = credit.getCreditAmountWithInterestRate();
        this.creditTerm = credit.getCreditTerm();
        this.creditOverpayment = credit.getCreditOverpayment();
        this.penalties = credit.getPenalties();
        this.creditPayments = creditPayments;
        this.creditStatus = credit.getCreditStatus();
    }

    public CreditDto(Long creditCardId, Long creditId, BigDecimal creditAmount, BigDecimal creditPay, BigDecimal creditOverpayment, BigDecimal penalties) {
        this.creditCardId = creditCardId;
        this.creditId = creditId;
        this.creditAmount = creditAmount;
        this.creditPay = creditPay;
        this.creditOverpayment = creditOverpayment;
        this.penalties = penalties;
    }

    public CreditStatus getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(CreditStatus creditStatus) {
        this.creditStatus = creditStatus;
    }

    public BigDecimal getCreditAmountWithInterestRate() {
        return creditAmountWithInterestRate;
    }

    public void setCreditAmountWithInterestRate(BigDecimal creditAmountWithInterestRate) {
        this.creditAmountWithInterestRate = creditAmountWithInterestRate;
    }

    public BigDecimal getCreditPayments() {
        return creditPayments;
    }

    public void setCreditPayments(BigDecimal creditPayments) {
        this.creditPayments = creditPayments;
    }

    public Date getCreditTerm() {
        return creditTerm;
    }

    public void setCreditTerm(Date creditTerm) {
        this.creditTerm = creditTerm;
    }

    public BigDecimal getCreditPay() {
        return creditPay;
    }

    public void setCreditPay(BigDecimal creditPay) {
        this.creditPay = creditPay;
    }

    public Long getCreditId() {
        return creditId;
    }

    public void setCreditId(Long creditId) {
        this.creditId = creditId;
    }

    public Long getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(Long creditCardId) {
        this.creditCardId = creditCardId;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public BigDecimal getCreditOverpayment() {
        return creditOverpayment;
    }

    public void setCreditOverpayment(BigDecimal creditOverpayment) {
        this.creditOverpayment = creditOverpayment;
    }

    public BigDecimal getPenalties() {
        return penalties;
    }

    public void setPenalties(BigDecimal penalties) {
        this.penalties = penalties;
    }
}
