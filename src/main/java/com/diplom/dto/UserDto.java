package com.diplom.dto;

import com.diplom.enums.Sex;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {
    @NotBlank
    @Pattern(regexp = "\\S+", message = "Must be min 4 non space symbol")
    @Size(min = 4, message = "Не меньше 4 знаков")
    private String username;
    @NotBlank
    @Pattern(regexp = "\\S+", message = "Must be min 4 non space symbol")
    @Size(min = 4, message = "Не меньше 4 знаков")
    private String password;
    @Email
    private String email;
    @NotBlank
    private String firstName;
    @NotBlank
    private String secondName;
    private String patronymic;
    private String birthDate;
    @Enumerated(EnumType.STRING)
    private Sex sex;

    public UserDto() {
    }

    public UserDto(@Size(min = 4, message = "Не меньше 4 знаков") String username, @Size(min = 4, message = "Не меньше 4 знаков") String password, @Email String email, @NotBlank String firstName, @NotBlank String secondName, String patronymic, String birthDate, Sex sex) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.secondName = secondName;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.sex = sex;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
