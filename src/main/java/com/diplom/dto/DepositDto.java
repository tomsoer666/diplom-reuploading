package com.diplom.dto;

import com.diplom.domain.Deposit;

import java.math.BigDecimal;
import java.sql.Date;

public class DepositDto {
    private Long id;
    private BigDecimal balance;
    private Date dateEnd;
    private BigDecimal minCash;
    private BigDecimal maxCash;
    private Integer month;
    private BigDecimal interestRate;
    private Boolean closable;
    private Boolean replenishable;
    private Boolean withdrawable;
    private Boolean capitalized;
    private Boolean automaticRenewal;

    public DepositDto(Deposit deposit) {
        this.id = deposit.getId();
        this.balance = deposit.getBalance();
        this.dateEnd = deposit.getDateEnd();
        this.minCash = deposit.getTariff().getMinCash();
        this.maxCash = deposit.getTariff().getMaxCash();
        this.month = deposit.getTariff().getMonth();
        this.interestRate = deposit.getTariff().getInterestRate();
        this.closable = deposit.getTariff().getClosable();
        this.replenishable = deposit.getTariff().getReplenishable();
        this.withdrawable = deposit.getTariff().getWithdrawable();
        this.capitalized = deposit.getTariff().getCapitalized();
        this.automaticRenewal = deposit.getAutomaticRenewal();
    }

    public DepositDto() {
    }

    public Boolean getAutomaticRenewal() {
        return automaticRenewal;
    }

    public void setAutomaticRenewal(Boolean automaticRenewal) {
        this.automaticRenewal = automaticRenewal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public BigDecimal getMinCash() {
        return minCash;
    }

    public void setMinCash(BigDecimal minCash) {
        this.minCash = minCash;
    }

    public BigDecimal getMaxCash() {
        return maxCash;
    }

    public void setMaxCash(BigDecimal maxCash) {
        this.maxCash = maxCash;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public Boolean getClosable() {
        return closable;
    }

    public void setClosable(Boolean closable) {
        this.closable = closable;
    }

    public Boolean getReplenishable() {
        return replenishable;
    }

    public void setReplenishable(Boolean replenishable) {
        this.replenishable = replenishable;
    }

    public Boolean getWithdrawable() {
        return withdrawable;
    }

    public void setWithdrawable(Boolean withdrawable) {
        this.withdrawable = withdrawable;
    }

    public Boolean getCapitalized() {
        return capitalized;
    }

    public void setCapitalized(Boolean capitalized) {
        this.capitalized = capitalized;
    }
}
