package com.diplom.dto;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class CashDto {
    private Long depositId;
    @Min(value = 0, message = "Must be positive")
    private BigDecimal cash;

    public CashDto() {
    }

    public CashDto(Long depositId) {
        this.depositId = depositId;
    }

    public CashDto(@Min(value = 0, message = "Must be positive") BigDecimal cash) {
        this.cash = cash;
    }

    public Long getDepositId() {
        return depositId;
    }

    public void setDepositId(Long depositId) {
        this.depositId = depositId;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }
}
