package com.diplom.dto;

import com.diplom.enums.CreditCardStatus;
import com.diplom.enums.MoneyType;
import com.diplom.enums.Sex;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Date;

public class CreditCardDto {
    private Long id;
    @Min(value = 0L, message = "must be positive")
    private BigDecimal creditLimitSum;
    @Min(value = 0, message = "must be positive")
    private BigDecimal interestRate;
    @Size(min = 6, max = 6)
    private String cardNumber;
    private Long accountNumber;
    @Enumerated(EnumType.STRING)
    private CreditCardStatus creditCardStatus;
    @Enumerated(EnumType.STRING)
    private MoneyType moneyType;
    @Email
    private String email;
    private String firstName;
    private String secondName;
    private String patronymic;
    private Date birthDate;
    @Enumerated(EnumType.STRING)
    private Sex sex;

    public CreditCardDto(Long id, @Min(value = 0L, message = "must be positive") BigDecimal creditLimitSum, @Min(value = 0, message = "must be positive") BigDecimal interestRate, Long accountNumber, CreditCardStatus creditCardStatus, @Email String email, @NotBlank String firstName, @NotBlank String secondName, String patronymic, Date birthDate, Sex sex) {
        this.id = id;
        this.creditLimitSum = creditLimitSum;
        this.interestRate = interestRate;
        this.accountNumber = accountNumber;
        this.creditCardStatus = creditCardStatus;
        this.email = email;
        this.firstName = firstName;
        this.secondName = secondName;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.sex = sex;
    }

    public CreditCardDto() {
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public MoneyType getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(MoneyType moneyType) {
        this.moneyType = moneyType;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCreditLimitSum() {
        return creditLimitSum;
    }

    public void setCreditLimitSum(BigDecimal creditLimitSum) {
        this.creditLimitSum = creditLimitSum;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public CreditCardStatus getCreditCardStatus() {
        return creditCardStatus;
    }

    public void setCreditCardStatus(CreditCardStatus creditCardStatus) {
        this.creditCardStatus = creditCardStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
