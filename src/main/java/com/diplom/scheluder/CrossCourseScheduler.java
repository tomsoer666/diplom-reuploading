package com.diplom.scheluder;

import com.diplom.util.MoneyRate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CrossCourseScheduler {

    private static BigDecimal random(int min, int max) {
        max -= min;
        return BigDecimal.valueOf((Math.random() * ++max) + min);
    }

    @Scheduled(cron = "0 0/30 * * * *")
    public void checkCourse() {
        MoneyRate.setDollarRate(random(60, 80));
        MoneyRate.setEuroRate(random(80, 110));
    }
}
