package com.diplom.scheluder;

import com.diplom.domain.Deposit;
import com.diplom.enums.DepositStatus;
import com.diplom.dao.DepositDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static com.diplom.util.DateUtil.MONTH;

@Component
public class DepositScheduler {
    private final DepositDao depositDao;

    @Autowired
    public DepositScheduler(DepositDao depositDao) {
        this.depositDao = depositDao;
    }

    @Scheduled(cron = "0 0 0 1 * ?")
    public void checkUnpaidCredits() {
        Set<Deposit> deposits = depositDao.getAllOpenedDeposits();
        long currentTime = System.currentTimeMillis();
        deposits.forEach(deposit -> {
            if (checkCashable(deposit)) {
                isCashable(deposit);
            } else if (checkCapitalizable(deposit)) {
                isCapitalizable(deposit, false);
            } else isNormal(deposit);
            checkDateOut(currentTime, deposit);
            depositDao.save(deposit);
        });
    }

    private boolean checkCapitalizable(Deposit deposit) {
        return Boolean.TRUE.equals(deposit.getTariff().getCapitalized());
    }

    private boolean checkCashable(Deposit deposit) {
        return Boolean.TRUE.equals(deposit.getTariff().getWithdrawable())
                || Boolean.TRUE.equals(deposit.getTariff().getReplenishable());
    }

    private void checkDateOut(long currentTime, Deposit deposit) {
        if (Instant.ofEpochMilli(deposit.getDateEnd().getTime()).toEpochMilli() <= currentTime) {
            checkAutomaticRenewal(deposit);
        }
    }

    private void checkAutomaticRenewal(Deposit deposit) {
        if (Boolean.TRUE.equals(deposit.getAutomaticRenewal())) {
            deposit.setDateEnd(new Date(Instant.now()
                    .plus(MONTH * deposit.getTariff().getMonth(), ChronoUnit.DAYS).toEpochMilli()));
        } else deposit.setDepositStatus(DepositStatus.CLOSED);
    }

    private void isCashable(Deposit deposit) {
        if (checkCapitalizable(deposit)) {
            isCapitalizable(deposit, true);
        } else {
            deposit.setBalance(deposit.getTariff().getInterestRate()
                    .divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_DOWN)
                    .multiply(deposit.getMinMonthBalance())
                    .add(deposit.getBalance()));
            deposit.setMinMonthBalance(deposit.getBalance());
        }
    }

    private void isNormal(Deposit deposit) {
        deposit.setBalance(deposit.getTariff().getInterestRate()
                .divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_DOWN)
                .multiply(deposit.getBalance())
                .add(deposit.getBalance()));
    }

    private void isCapitalizable(Deposit deposit, Boolean isCashable) {
        if (Boolean.TRUE.equals(isCashable)) {
            deposit.setCapitalizeRate(deposit.getTariff().getInterestRate()
                    .divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_DOWN)
                    .multiply(deposit.getMinMonthBalance())
                    .add(deposit.getCapitalizeRate()));
            deposit.setBalance(deposit.getBalance().add(deposit.getCapitalizeRate()));
            deposit.setMinMonthBalance(deposit.getBalance());
        } else {
            deposit.setCapitalizeRate(deposit.getTariff().getInterestRate()
                    .divide(BigDecimal.valueOf(12), 2, RoundingMode.HALF_DOWN)
                    .multiply(deposit.getBalance())
                    .add(deposit.getCapitalizeRate()));
            deposit.setBalance(deposit.getBalance().add(deposit.getCapitalizeRate()));
        }
    }
}
