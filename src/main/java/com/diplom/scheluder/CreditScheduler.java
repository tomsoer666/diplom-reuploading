package com.diplom.scheluder;

import com.diplom.domain.Credit;
import com.diplom.domain.CreditCard;
import com.diplom.enums.CreditCardStatus;
import com.diplom.enums.CreditStatus;
import com.diplom.repository.CreditCardRepository;
import com.diplom.dao.CreditCardDao;
import com.diplom.dao.CreditDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Set;

import static com.diplom.util.MaximumPenalties.MAX_PENALTIES;

@Component
public class CreditScheduler {
    private final CreditDao creditDao;
    private final CreditCardDao creditCardDao;

    @Autowired
    public CreditScheduler(CreditDao creditDao, CreditCardDao creditCardDao, CreditCardRepository creditCardRepository) {
        this.creditDao = creditDao;
        this.creditCardDao = creditCardDao;
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void checkUnpaidCredits() {
        Set<Credit> credits = creditDao.findByCreditStatus(CreditStatus.NORMAL);
        long currentTime = System.currentTimeMillis();
        credits.forEach(credit -> {
            if (Instant.ofEpochMilli(credit.getCreditTerm().getTime()).toEpochMilli()
                    < currentTime) {
                credit.setCreditStatus(CreditStatus.OUT_DATE);
                creditDao.save(credit);
            }
        });
    }

    @Scheduled(cron = "0 10 0 * * ?")
    public void checkClosingCreditCards() {
        Set<CreditCard> cards = creditCardDao.findByCreditCardStatus(CreditCardStatus.ACCEPT_CREDIT_CARD_CLIENT);
        removeActualCreditCard(cards);
        closeNotActualCard(cards);
    }

    @Scheduled(cron = "0 25 0 * * ?")
    public void setPenalties() {
        Set<Credit> credits = creditDao.findByCreditStatus(CreditStatus.OUT_DATE);
        credits.forEach(credit -> {
            BigDecimal overpayment = credit.getCreditAmount().multiply(credit.getPenalties());
            if (credit.getCreditAmount().add(overpayment.add(credit.getCreditOverpayment()))
                    .compareTo(credit.getCreditAmount().multiply(MAX_PENALTIES)) < 0) {
                credit.setCreditOverpayment(overpayment.add(credit.getCreditOverpayment()));
                creditDao.save(credit);
            }
        });
    }

    private void closeNotActualCard(Set<CreditCard> cards) {
        for (CreditCard creditCard : cards) {
            Set<Credit> credits = creditDao
                    .findByCreditCardAndCreditStatus(creditCard, CreditStatus.OUT_DATE);
            if (credits.isEmpty()) {
                creditCard.setCreditCardStatus(CreditCardStatus.CLOSE_CREDIT_CARD);
                creditCardDao.save(creditCard);
            }
        }
    }

    private void removeActualCreditCard(Set<CreditCard> cards) {
        long currentTime = System.currentTimeMillis();
        cards.forEach(creditCard -> {
            if (Instant.ofEpochMilli(creditCard.getDateEnd().getTime()).toEpochMilli() > currentTime) {
                cards.remove(creditCard);
            }
        });
    }
}
