package com.diplom.controller.operator.credit;

import com.diplom.dao.CreditCardDao;
import com.diplom.dao.CreditDao;
import com.diplom.domain.Bank;
import com.diplom.domain.CreditCard;
import com.diplom.dto.CreditCardDto;
import com.diplom.dto.CreditDto;
import com.diplom.service.BankService;
import com.diplom.service.CreditCardService;
import com.diplom.service.DtoService;
import com.diplom.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.REDIRECT_OPERATOR;

@Controller
public class OperatorCreditController {
    private final OperatorService operatorService;
    private final CreditCardService creditCardService;
    private final CreditCardDao creditCardDao;
    private final BankService bankService;
    private final CreditDao creditDao;
    private final DtoService dtoService;

    @Autowired
    public OperatorCreditController(OperatorService operatorService, CreditCardService creditCardService, CreditCardDao creditCardDao, BankService bankService, CreditDao creditDao, DtoService dtoService) {
        this.operatorService = operatorService;
        this.creditCardService = creditCardService;
        this.creditCardDao = creditCardDao;
        this.bankService = bankService;
        this.creditDao = creditDao;
        this.dtoService = dtoService;
    }

    @GetMapping("/operator/requestCreditCard")
    public String getCreditCardRequests(Model model) {
        model.addAttribute("creditCards",
                dtoService.mappingCreditCardDto(creditCardService.showRequestOnCreditCard()));
        return "operator/credit/operatorRequestCreditCard";
    }

    @GetMapping("/operator/acceptCreditCard/{creditCardId}")
    public String registerBankAccount(@Valid @PathVariable("creditCardId") Long creditCardId,
                                      Model model) {
        model.addAttribute("creditCard", creditCardDao.findCreditCardForAccept(creditCardId));
        return "operator/credit/operatorAcceptCreditCard";
    }

    @GetMapping("/operator/changeCommission")
    public String getBanks(Model model) {
        model.addAttribute("banks", bankService.getAllBank());
        return "operator/credit/operatorChangeCommission";
    }

    @GetMapping("/operator/changePenalties")
    public String getCredits(Model model) {
        model.addAttribute("credits", creditDao.getCreditsExceptClosed());
        return "operator/credit/operatorChangePenalties";
    }


    @GetMapping("/operator/editPenalties/{creditId}")
    public String editPenalties(@Valid @PathVariable("creditId") Long creditId,
                                Model model) {
        model.addAttribute("credit", dtoService.findByIdExceptClosed(creditId));
        return "operator/credit/operatorEditPenalties";
    }

    @PostMapping("/operator/editPenalties")
    public String acceptPenalties(@ModelAttribute("credit") @Valid CreditDto creditDto,
                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "operator/credit/operatorEditPenalties";
        }
        operatorService.changeCreditPenalties(creditDto);
        return REDIRECT_OPERATOR;
    }

    @GetMapping("/operator/choiceBank/{bankId}")
    public String chooseBank(@Valid @PathVariable("bankId") Long bankId,
                             Model model) {
        model.addAttribute("bank", bankService.getBankById(bankId));
        return "operator/credit/operatorChooseBank";
    }

    @PostMapping("/operator/acceptCommission")
    public String acceptCommission(@ModelAttribute("bank") @Valid Bank bank,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "operator/credit/operatorChooseBank";
        }
        operatorService.changeBankCommission(bank);
        return REDIRECT_OPERATOR;
    }

    @PostMapping("/operator/acceptCreditCard")
    public String acceptCreditCard(@ModelAttribute("creditCard") @Valid CreditCardDto creditCardDto,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "operator/credit/operatorAcceptCreditCard";
        }
        operatorService.acceptCreditCard(creditCardDto);
        return REDIRECT_OPERATOR;
    }

    @PostMapping("/operator/declineCreditCard")
    public String declineCreditCard(@Valid @RequestParam("creditCard") Long creditCardId) {
        operatorService.declineCreditCard(creditCardId);
        return REDIRECT_OPERATOR;
    }
}
