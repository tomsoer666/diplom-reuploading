package com.diplom.controller.operator;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OperatorController {
    @GetMapping("/operator")
    public String getOperatorMain() {
        return "operator/operatorMain";
    }
}
