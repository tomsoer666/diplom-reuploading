package com.diplom.controller.operator.transfer;

import com.diplom.service.OperatorService;
import com.diplom.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.TRANSFER;

@Controller
public class OperatorTransferController {
    private final OperatorService operatorService;
    private final TransferService transferService;

    @Autowired
    public OperatorTransferController(OperatorService operatorService, TransferService transferService) {
        this.operatorService = operatorService;
        this.transferService = transferService;
    }

    @GetMapping("/operator/transfers")
    public String transfers(Model model) {
        model.addAttribute("transfers", transferService.getSuspiciousTransfers());
        return "operator/transfer/operatorTransfers";
    }

    @PostMapping("/operator/acceptTransfer")
    public String acceptTransfer(@Valid @RequestParam(TRANSFER) Long transferId) {
        operatorService.acceptTransfer(transferId);
        return "redirect:/operator/transfers";
    }

    @PostMapping("/operator/declineTransfer")
    public String declineTransfer(@Valid @RequestParam(TRANSFER) Long transferId) {
        operatorService.declineTransfer(transferId);
        return "redirect:/operator/transfers";
    }
}
