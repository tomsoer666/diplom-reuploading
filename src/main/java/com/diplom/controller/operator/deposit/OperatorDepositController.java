package com.diplom.controller.operator.deposit;

import com.diplom.domain.Tariff;
import com.diplom.dao.TariffDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.OPERATOR_EDIT_TARIFF;

@Controller
public class OperatorDepositController {
    private final TariffDao tariffDao;

    @Autowired
    public OperatorDepositController(TariffDao tariffDao) {
        this.tariffDao = tariffDao;
    }

    @GetMapping("/operator/tariffs")
    public String tariffs(Model model) {
        model.addAttribute("tariffs", tariffDao.getAllTariffs());
        return "operator/deposit/operatorTariffs";
    }

    @GetMapping("/operator/addTariff")
    public String addTariff(Model model) {
        model.addAttribute("tariff", tariffDao.emptyTariff());
        return OPERATOR_EDIT_TARIFF;
    }

    @GetMapping("/operator//editTariff/{tariffId}")
    public String registerBankAccount(@Valid @PathVariable("tariffId") Long tariffId,
                                      Model model) {
        model.addAttribute("tariff", tariffDao.findById(tariffId));
        return OPERATOR_EDIT_TARIFF;
    }

    @PostMapping("/operator/saveTariff")
    public String saveTariff(@ModelAttribute("tariff") @Valid Tariff tariff,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return OPERATOR_EDIT_TARIFF;
        }
        tariffDao.save(tariff);
        return "redirect:/operator/tariffs";
    }

    @PostMapping("/operator/deleteTariff")
    public String deleteTariff(@Valid @RequestParam("tariff") Long tariffId) {
        tariffDao.delete(tariffId);
        return "redirect:/operator";
    }
}
