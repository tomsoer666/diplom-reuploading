package com.diplom.controller.client.transfer;

import com.diplom.dto.TransferDto;
import com.diplom.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.REDIRECT_USER;
import static com.diplom.util.UrlPath.TRANSFER;

@Controller
public class ClientTransferController {
    private final TransferClientService clientService;
    private final BankAccountService bankAccountService;
    private final TransferService transferService;
    private final CreditCardService creditCardService;

    @Autowired
    public ClientTransferController(TransferClientService clientService, BankAccountService bankAccountService, TransferService transferService, CreditCardService creditCardService) {
        this.clientService = clientService;
        this.bankAccountService = bankAccountService;
        this.transferService = transferService;
        this.creditCardService = creditCardService;
    }

    @GetMapping("/user/openTransfer")
    public String openTransfer() {
        return "client/transfer/clientChooseTransfer";
    }

    @GetMapping("/user/chooseAccountForTransfer")
    public String chooseAccountForTransfer(Model model) {
        model.addAttribute("bankAccounts", bankAccountService.getBankAccountByUser());
        return "client/transfer/clientAccountForTransfer";
    }

    @GetMapping("/user/chooseCardForTransfer")
    public String chooseCardForTransfer(Model model) {
        model.addAttribute("creditCards", creditCardService.showAcceptedCreditCard());
        return "client/transfer/clientCardForTransfer";
    }

    @GetMapping("/user/transferAccountTo/{bankAccountId}")
    public String transferAccountTo(@Valid @PathVariable("bankAccountId") Long bankAccountId,
                                    Model model) {
        model.addAttribute("bankAccountId", bankAccountId);
        return "client/transfer/clientAccountTransfer";
    }

    @GetMapping("/user/transferOnAccount/{bankAccountId}")
    public String transferOnAccount(@Valid @PathVariable("bankAccountId") Long bankAccountId,
                                    Model model) {
        TransferDto transferDto = new TransferDto();
        transferDto.setSenderAccount(bankAccountId);

        model.addAttribute(TRANSFER, transferDto);
        return "client/transfer/clientAccountToAccount";
    }

    @GetMapping("/user/transferOnCard/{bankAccountId}")
    public String transferOnCard(@Valid @PathVariable("bankAccountId") Long bankAccountId,
                                 Model model) {
        TransferDto transferDto = new TransferDto();
        transferDto.setSenderAccount(bankAccountId);
        model.addAttribute(TRANSFER, transferDto);
        return "client/transfer/clientAccountToCard";
    }

    @PostMapping("/user/accToAcc")
    public String accountToAccount(@ModelAttribute(TRANSFER) @Valid TransferDto transferDto,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/transfer/clientAccountToAccount";
        }
        clientService.accountToAccountTransfer(transferDto);
        return REDIRECT_USER;
    }

    @PostMapping("/user/accToCard")
    public String accountToCard(@ModelAttribute(TRANSFER) @Valid TransferDto transferDto,
                                BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/transfer/clientAccountToCard";
        }
        clientService.accountToCardTransfer(transferDto);
        return REDIRECT_USER;
    }

    @PostMapping("/user/cardToAcc")
    public String cardToAccount(@ModelAttribute(TRANSFER) @Valid TransferDto transferDto,
                                BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/transfer/clientCardToAccount";
        }
        clientService.cardToAccountTransfer(transferDto);
        return REDIRECT_USER;
    }

    @PostMapping("/user/cardToCard")
    public String cardToCard(@ModelAttribute(TRANSFER) @Valid TransferDto transferDto,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/transfer/clientCardToCard";
        }
        clientService.cardToCardTransfer(transferDto);
        return REDIRECT_USER;
    }

    @GetMapping("/user/transferCardTo/{creditCardId}")
    public String transferCardTo(@Valid @PathVariable("creditCardId") Long creditCardId,
                                 Model model) {
        model.addAttribute("creditCardId", creditCardId);
        return "client/transfer/clientCardTransfer";
    }

    @GetMapping("/user/transferCardOnAccount/{creditCardId}")
    public String transferCardOnAccount(@Valid @PathVariable("creditCardId") Long creditCardId,
                                        Model model) {
        TransferDto transferDto = new TransferDto();
        transferDto.setSenderCard(creditCardId);
        model.addAttribute(TRANSFER, transferDto);
        return "client/transfer/clientCardToAccount";
    }

    @GetMapping("/user/transferCardOnCard/{creditCardId}")
    public String transferCardOnCard(@Valid @PathVariable("creditCardId") Long creditCardId,
                                     Model model) {
        TransferDto transferDto = new TransferDto();
        transferDto.setSenderCard(creditCardId);
        model.addAttribute(TRANSFER, transferDto);
        return "client/transfer/clientCardToCard";
    }

    @GetMapping("/user/transfers")
    public String transfers(Model model) {
        model.addAttribute("transfers", transferService.getTransfers());
        return "client/transfer/clientTransfers";
    }

}
