package com.diplom.controller.client.credit;

import com.diplom.dto.CreditCardDto;
import com.diplom.dto.CreditDto;
import com.diplom.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.CREDIT_CARDS;
import static com.diplom.util.UrlPath.REDIRECT_USER;

@Controller
public class ClientCreditController {
    private final CreditClientService clientService;
    private final BankAccountService bankAccountService;
    private final BankService bankService;
    private final DtoService dtoService;
    private final CreditCardService creditCardService;

    @Autowired
    public ClientCreditController(CreditClientService clientService, BankAccountService bankAccountService, BankService bankService, DtoService dtoService, CreditCardService creditCardService) {
        this.bankAccountService = bankAccountService;
        this.bankService = bankService;
        this.clientService = clientService;
        this.dtoService = dtoService;
        this.creditCardService = creditCardService;
    }

    @GetMapping("/user/openBankAccounts")
    public String getBankAccounts(Model model) {
        model.addAttribute("banks", bankService.getAllBank());
        return "client/credit/clientBankAccounts";
    }

    @GetMapping("/user/openCreditCard")
    public String openCreditCard(Model model) {
        model.addAttribute("bankAccounts", bankAccountService.getBankAccountByUser());
        model.addAttribute("moneyType", dtoService.getCreditCardDto());
        return "client/credit/clientCreditCards";
    }

    @GetMapping("/user/checkCreditCard")
    public String checkCreditCard(Model model) {
        model.addAttribute(CREDIT_CARDS, creditCardService.showRequestedCreditCard());
        return "client/credit/clientRequestCreditCard";
    }

    @GetMapping("/user/chooseCardsForCredit")
    public String chooseCardForCredit(Model model) {
        model.addAttribute(CREDIT_CARDS, creditCardService.showAcceptedCreditCard());
        return "client/credit/clientChooseCardForCredit";
    }

    @GetMapping("/user/chooseCardsForRepay")
    public String chooseCardForRepay(Model model) {
        model.addAttribute(CREDIT_CARDS, creditCardService.showAcceptedCreditCard());
        return "client/credit/clientChooseCardForRepay";
    }

    @GetMapping("/user/chooseCreditCardForRepay/{creditCardId}")
    public String chooseCreditForRepay(@Valid @PathVariable("creditCardId") Long creditCardId,
                                       Model model) {
        model.addAttribute("credits", dtoService.findOpenedCredit(creditCardId));
        return "client/credit/clientCredits";
    }

    @GetMapping("/user/repayCredit/{creditId}")
    public String repayCreditPage(@Valid @PathVariable("creditId") Long creditId,
                                  Model model) {
        model.addAttribute("credit", dtoService.mappingCreditDto(creditId));
        return "client/credit/clientRepayCredit";
    }

    @GetMapping("/user/editCreditTerm/{creditCardId}")
    public String editCreditTerm(@Valid @PathVariable("creditCardId") Long creditCardId,
                                 Model model) {
        CreditDto creditDto = new CreditDto();
        creditDto.setCreditCardId(creditCardId);
        model.addAttribute("credit", creditDto);
        return "client/credit/clientEditCreditTerm";
    }

    @PostMapping("/user/acceptCreditCard")
    public String acceptCreditCard(@Valid @RequestParam("creditCard") Long creditCardId) {
        clientService.acceptCreditCard(creditCardId);
        return REDIRECT_USER;
    }

    @PostMapping("/user/declineCreditCard")
    public String declineCreditCard(@Valid @RequestParam("creditCard") Long creditCardId) {
        clientService.declineCreditCard(creditCardId);
        return REDIRECT_USER;
    }

    @PostMapping("/user/openCredit")
    public String acceptCredit(@ModelAttribute("credit") @Valid CreditDto creditDto,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/credit/clientEditCreditTerm";
        }
        clientService.openCredit(creditDto);
        return REDIRECT_USER;
    }

    @PostMapping("/user/repayCredit")
    public String repayCredit(@ModelAttribute("credit") @Valid CreditDto creditDto,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/credit/clientRepayCredit";
        }
        clientService.repayCredit(creditDto);
        return REDIRECT_USER;
    }

    @PostMapping("/user/choiceBank")
    public String registerBankAccount(@Valid @RequestParam("bank") Long bankId) {
        bankAccountService.openingBankAccount(bankId);
        return REDIRECT_USER;
    }

    @PostMapping("/user/creditCardRequest")
    public String requestCreditCard(@Valid @RequestParam("bankAccount") Long bankAccountId,
                                    @ModelAttribute("moneyType") @Valid CreditCardDto creditCardDto) {
        creditCardService.openCreditCard(bankAccountId, creditCardDto);
        return REDIRECT_USER;
    }
}
