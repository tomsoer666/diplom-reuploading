package com.diplom.controller.client;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ClientController {
    @GetMapping("/user")
    public String getClientMain() {
        return "client/clientMain";
    }
}
