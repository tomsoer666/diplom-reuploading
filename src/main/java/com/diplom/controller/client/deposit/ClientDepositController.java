package com.diplom.controller.client.deposit;

import com.diplom.dao.DepositDao;
import com.diplom.dao.TariffDao;
import com.diplom.dto.CashDto;
import com.diplom.exceptions.IllegalCashException;
import com.diplom.service.DepositClientService;
import com.diplom.service.DtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.REDIRECT_USER_DEPOSITS;

@Controller
public class ClientDepositController {
    private final DepositClientService clientService;
    private final TariffDao tariffDao;
    private final DepositDao depositDao;
    private final DtoService dtoService;

    @Autowired
    public ClientDepositController(DepositClientService clientService, TariffDao tariffDao, DepositDao depositDao, DtoService dtoService) {
        this.clientService = clientService;
        this.tariffDao = tariffDao;
        this.depositDao = depositDao;
        this.dtoService = dtoService;
    }

    @GetMapping("/user/deposits")
    public String deposits(Model model) {
        model.addAttribute("deposits", depositDao.getOpenDeposits());
        return "client/deposit/clientDeposits";
    }

    @GetMapping("/user/addDeposit")
    public String addDeposit(Model model) {
        model.addAttribute("tariffs", tariffDao.getAllTariffs());
        model.addAttribute("cash", dtoService.getCashDto());
        return "client/deposit/clientOpenDeposit";
    }

    @PostMapping("/user/openDeposit")
    public String acceptCreditCard(@Valid @RequestParam("tariff") Long tariffId,
                                   @ModelAttribute("cash") @Valid CashDto cashDto,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new IllegalCashException("Cash must be positive");
        }
        clientService.openDeposit(tariffId, cashDto.getCash());
        return REDIRECT_USER_DEPOSITS;
    }

    @GetMapping("/user/replenishDeposit/{depositId}")
    public String replenishDeposit(@Valid @PathVariable("depositId") Long depositId,
                                   Model model) {
        if (!clientService.isReplenishable(depositId)) {
            return REDIRECT_USER_DEPOSITS;
        }
        model.addAttribute("cash", dtoService.getCashDto(depositId));
        return "client/deposit/clientReplenishDeposit";
    }

    @PostMapping("/user/replenishDeposit")
    public String replenishDeposit(@ModelAttribute("cash") @Valid CashDto cashDto,
                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/deposit/clientReplenishDeposit";
        }
        clientService.replenishDeposit(cashDto);
        return REDIRECT_USER_DEPOSITS;
    }

    @GetMapping("/user/withdrawDeposit/{depositId}")
    public String withdrawDeposit(@Valid @PathVariable("depositId") Long depositId,
                                  Model model) {
        if (!clientService.isWithdrawable(depositId)) {
            return REDIRECT_USER_DEPOSITS;
        }
        model.addAttribute("cash", dtoService.getCashDto(depositId));
        return "client/deposit/clientWithdrawDeposit";
    }

    @PostMapping("/user/withdrawDeposit")
    public String withdrawDeposit(@ModelAttribute("cash") @Valid CashDto cashDto,
                                  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "client/deposit/clientWithdrawDeposit";
        }
        clientService.withdrawDeposit(cashDto);
        return REDIRECT_USER_DEPOSITS;
    }

    @PostMapping("/user/closeDeposit")
    public String closeDeposit(@Valid @RequestParam("deposit") Long depositId) {
        clientService.closeDeposit(depositId);
        return REDIRECT_USER_DEPOSITS;
    }

    @PostMapping("/user/automaticRenewalDeposit")
    public String automaticRenewalDeposit(@Valid @RequestParam("deposit") Long depositId) {
        clientService.automaticRenewalDeposit(depositId);
        return REDIRECT_USER_DEPOSITS;
    }
}
