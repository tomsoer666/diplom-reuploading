package com.diplom.controller;

import com.diplom.dto.UserDto;
import com.diplom.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import static com.diplom.util.UrlPath.REGISTRATION;

@Controller
public class RegistrationController {

    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("user", new UserDto());
        return REGISTRATION;
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("user") @Valid UserDto user,
                          BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return REGISTRATION;
        }
        if (!userService.saveUser(user)) {
            model.addAttribute("usernameError",
                    "Пользователь с таким именем уже существует");
            return REGISTRATION;
        }
        return "redirect:/";
    }
}
